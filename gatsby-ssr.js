/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

// You can delete this file if you're not using it
import * as React from "react"

export const onRenderBody = ({ setHeadComponents }) => {
    setHeadComponents([
        <link
            rel="preload"
            href="/fonts/Vollkorn-Italic.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="anonymous"
            key="interFont"
        />,
        <link
            rel="preload"
            href="/fonts/Vollkorn-Regular.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="anonymous"
            key="interFont"
        />,
        <link
            rel="preload"
            href="/fonts/slick.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="anonymous"
            key="interFont"
        />,
    ])
}