---
path: "/about"
title: "About Me"
image: '/content/images/about/about-hero-1.png'
---
# About Me
I am passionate about creative solutions. I believe the user-centric approach is the only logical way to build digital products. It ensures success for both the users and businesses that cater to them. Aligning business goals with user needs is therefore critical to identifying and solving the right problem that benefits everyone.

I feel all my knowledge and experience in visual design and programming comes full circle in UX Design. With adoption of core UX methodologies through my recent work, I appreciate user as a critical - indeed a driving factor in building products that can truly make a difference.

I am inspired to build such products and continue my journey as a student of design - discovering great problems and solving them with the user need at the center of the entire creative process.