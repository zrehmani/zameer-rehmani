const theme = {
  colors: {
    primary: "#E91E63",
    lightPrimary: "#F8BBD0",
    darkPrimary: '#C2185B',
    white: "#FFFFFF",
    accent: "#E040FB",
    // primaryText: "#212121",
    primaryText: "#4A4A4A",
    secondaryText: "#757575",
    lightText:'#9B9B9B',
    legacyText: '#676667',
    altText: '#777',
    divider: "#DDD",
    shadow: '#BDBDBD',
    border: '#EBEBEB',
    medBorder:'#DDD',
    darkBorder: '#979797',
    blue: '#F3F9FF',
    blue1: '#4A90E2',
    lightBlue: '#F5FAFF',
  },
  fonts: {
    preview: {
      title: 'Helvetica Neue, Avenir Next, Segoe UI, Helvetica, Arial, sans-serif'
    }
  }
}

export default theme;