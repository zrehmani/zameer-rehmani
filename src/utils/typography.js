import Typography from "typography"

const typography = new Typography({
  baseFontSize: '16px',
  baseLineHeight: 1.666,
  googleFonts: [
    { name: 'Vollkorn',
      styles: [
        'Regular,Italic'
      ]
    }
  ],
  includeNormalize: true,
  headerFontFamily: ['Helvetica Neue', 'Avenir Next', 'Segoe UI', 'Helvetica', 'Arial', 'sans-serif'],
  bodyFontFamily: ['Helvetica Neue', 'Avenir Next', 'Segoe UI', 'Helvetica', 'Arial', 'sans-serif'],
  overrideStyles: ({ adjustFontSizeTo, rhythm }, options, styles) => ({
    'blockquote > pre': {
        fontFamily: 'Volkorn, serif',
        fontStyle: 'italic',
        paddingLeft: rhythm(13/16),
        marginLeft: rhythm(-1),
    },
    'blockquote > :last-child': {
      marginBottom: 0,
    },
  })
});
export default typography