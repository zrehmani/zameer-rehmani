import React from 'react';
import ClientsImage from '../images/clients@2x.png';
import styled from '@emotion/styled';

const Container = styled('div') `
  width: 100%;
  margin: 0 auto;
  & h3 {
    font-weight: 500;
    margin-bottom: 1rem;
  }
`

class Clients extends React.Component {
  render () {
    return (
      <Container>
        <h3>Clients</h3>
        <img src={ClientsImage} alt="Clients"/>
      </Container>
    )
  }
}

export default Clients