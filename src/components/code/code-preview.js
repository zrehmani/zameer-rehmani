import React from 'react';
import styled from '@emotion/styled';
import { Link } from 'gatsby';

const Container = styled ('div')`

    font-size: 1.125rem;
    
    & .inner {
      width: 25%;
      padding: 0 1rem;
      display: inline-block;
    }
    
    & a {
      text-decoration: none;
      color: #676667;
      
    }
    
    & img {
      margin: 0 auto;
      max-width: 302px;
      width: 100%;
      border: 1px solid rgb(192, 192, 192);
    }
    
    & .label {
      padding: .25rem;
      font-size: 1.25rem;
    }
    
    @media (max-width: 840px) {
      & .inner {
        padding: 0 .25rem;
      }
    }
    
    @media (min-width: 840px) and (max-width: 960px) {
       & .inner {
        padding: 0 .5rem;
      }
    }
    
    @media (min-width: 960px) and (max-width: 1280px) {
      & .inner {
        padding: 0 .75rem;
      }
    }
    
    @media (min-width: 1400px) {
      text-align: left;
    }
    
`

let getItem = (data) => {
  return data.map((item) => {
    return (
      <div className={'inner'}>
        <Link to={item.link} className={'link'}>
          <img src={item.img} alt=""/>
          <p className={'label'}>{item.label}</p>
        </Link>
      </div>
    )
  });
}

class CodePreview extends React.Component {
  render() {

    return (
      <Container>{getItem(this.props.data)}</Container>
    )
  }
}

export default CodePreview