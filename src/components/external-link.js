import React from 'react';
import LinkIconImage from '../images/link-icon.png';
import styled from '@emotion/styled';

const Container = styled('div')`

  a {
    color: ${props => (props.theme.colors.legacyText)};
    :hover {
      color: ${props => (props.theme.colors.primary)};;
    }
  }
  

`

const para = {
  float: 'left',
  fontSize:'1rem',
  paddingLeft:'8px',
  margin: 0,
  fontWeight: '500'
}

const image = {
  margin: '0',
  float: 'left',
  width:'18px',
  marginTop:'3px',
}

class ExternalLink extends React.Component {
  render () {
    let data = this.props.data;
    return (
      <Container>
        <a href={data.link} target="_blank" rel="noopener noreferrer">
          <div>
            <img src={LinkIconImage} alt="" style={image}/>
            <p style={para}>{data.label}</p>
          </div>
        </a>
      </Container>
    )
  }
}

export default ExternalLink;