import React from 'react';
import styled from '@emotion/styled';
import { Link } from 'gatsby';

const Container = styled ('div')`

    margin: 0 auto;
    max-width: 1440px;
    border-top: 1px solid rgb(220, 220, 220);
    
    background-color: ${props => props.odd ? 'rgb(244,249,250)' : '#FCFCFC'};
    
    & .copy {
      float: left;
      width: 36.5%;
      padding: 1rem;
      font-size: 1.4rem;
    }
    
    & .left {
      margin-top: 1%;
      margin-bottom: 1%;
      border-bottom: 1px solid rgb(151,151,151);
    }
    
    & .right {
      margin-top: 12%;
      margin-bottom: 7%;
      border-top: 1px solid rgb(151,151,151);
    }
    
    & .link {
      color: rgb(74,74,74);
    }
    
    & .inner {
      margin: 0 auto;
      padding: 4.3rem 5rem .5rem 5rem;
    }
    
    & .image-container {
      float: left;
      width: 27%;
      padding: 0 1rem;
    }
    
    & img {
      margin: 0 auto;
      display: block;
    }
    
    & .label {
      text-align: center;
      padding: .25rem;
      font-size: 2rem;
      margin: 0 auto;
      & span {
        display: block;
      }
    }
    
    @media (max-width: 840px) {
      & .copy {
        display: none;
      }
      & .image-container {
        width: 100%;
      }
      & .label {
        font-size: 2rem;
      }
    }
    
    @media (min-width: 840px) and (max-width: 960px) {
        & .right {
          margin-top: 8%;
        }
        & .label {
          font-size: 1.5rem;
        }
    }
    
    @media (min-width: 960px) and (max-width: 1280px) {
        & .right {
          margin-top: 10%;
        }
        & .label {
          font-size: 1.5rem;
        }
    }
    
`

class UXPreview extends React.Component {
  render() {
    let data = this.props.data;
    return (
      <Container odd={this.props.odd}>
        <Link to={data.link} className={'link'}>
          <div className={'inner'}>
            <div className={'copy left'}>{data.copy1}</div>
            <div className={'image-container'}>
              <img src={data.img} alt=""/>
              <p className={'label'}>{data.label}<span>{data.label2}</span></p>
            </div>
            <div className={'copy right'}>{data.copy2}</div>
            <div className={'clear'} />
          </div>
        </Link>
      </Container>
    )
  }
}

export default UXPreview