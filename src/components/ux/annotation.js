import React from 'react';
import styled from '@emotion/styled'
import BulletImage1 from '../../images/bullet-1.png';
import BulletImage2 from '../../images/bullet-2.png';
import BulletImage3 from '../../images/bullet-3.png';
import BulletImage4 from '../../images/bullet-4.png';
import BulletImage5 from '../../images/bullet-5.png';
import BulletImage6 from '../../images/bullet-6.png';
import BulletImage7 from '../../images/bullet-7.png';
import BulletImage8 from '../../images/bullet-8.png';

const Container = styled ('div') ({
  marginLeft: '-2rem',
  marginTop: '2rem',
  h3 : {
    fontSize: '2rem',
    fontWeight: '500',
    paddingLeft: '1.5rem'
  },
  span: {
    fontSize: '1.3125rem',
  },
  ul : {
    listStyle: 'none',
    li: {
      listStylePosition: 'outside',
      paddingBottom: '1rem',
      img: {
        marginLeft:'-4rem'
      },
      span: {
        display: 'block',
        marginTop: '-4.7rem'
      }
    }
  }
})

class Annotation extends React.Component {
  render () {
    let images = [BulletImage1, BulletImage2, BulletImage3, BulletImage4, BulletImage5, BulletImage6, BulletImage7, BulletImage8];
    let offset=this.props.offset;
    let idx = (offset) ? offset : 0;
    let img = images[idx];
    let list = this.props.data.list.map( (item, i) => {
      img = images[idx];
      idx++;
      return <li key={idx}><img src={img} alt={""}/><span>{item}</span></li>
    })

    return (
      <Container>
        <h3>{this.props.data.title}</h3>
        <ul>{list}</ul>
      </Container>
    )
  }
}

export default Annotation;