import React from 'react';


class BigPrototypeLink extends React.Component {
  render () {
    return (
      <div style={{textAlign:'center', maxWidth:'650px', margin: '0 auto', borderTop: '1px solid rgba(192, 192, 192, 1)', paddingTop:'5rem'}}>
        <a href={this.props.link} target="_blank" rel="noopener noreferrer" style={{textDecoration:"none", color: "#676667"}} >
          <div>
            <img src={this.props.img} alt="" style={{margin: '0'}}/>
            <p style={{fontSize:'2.625rem'}}>Prototype</p>
          </div>
        </a>
      </div>
    )
  }
}

export default BigPrototypeLink;