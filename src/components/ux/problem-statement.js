import React from 'react'
import styled from '@emotion/styled'

const Container = styled ('div') ({
  borderTop: '1px solid rgba(220, 220, 220, 1)',
  borderBottom: '1px solid rgba(220, 220, 220, 1)',
  marginTop: '2rem',
  paddingTop: '3rem',
  paddingBottom: '3rem',
  h3 : {
    fontWeight: 'normal',
    fontSize: '1.75rem',
    width: '15rem',
    lineHeight: '1.2',
  },
  p: {
    margin: '0',
    width: '75.4%',
    float: 'left',
    fontSize: '1.75rem'
  },
  span: {
    fontWeight: '600',
    fontStyle: 'italic',
    display: 'block'
  }
})

class ProblemStatement extends React.Component {
  render () {
    return (
      <Container className={"row"}>
        <div className={"col1"} >
          <h3>PROBLEM STATEMENT</h3>
        </div>
        <p>{this.props.statement}
          <span>{this.props.opportunity}</span></p>
        <div className={"clear"} />
      </Container>
    )
  }
}

export default ProblemStatement