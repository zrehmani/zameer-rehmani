import React from 'react';
import styled from '@emotion/styled'
import QuoteImg from '../../images/quote-right.png';

const Container = styled ('div') ({
  position: 'relative',
  marginTop: '1rem',
})

const UserImage = styled('div') ({
  position: 'absolute',
  zIndex: '500',
  left: '5rem',
  top: '-120px',
})

const InnerQuote = styled ('div') (props => ({
  position: 'absolute',
  zIndex: '100',
  backgroundColor: "rgba(244, 249, 250, 1)",
  borderTop: '1px solid rgba(192, 192, 192, 1)',
  borderBottom: '1px solid rgba(192, 192, 192, 1)',
  marginBottom: '2rem',
  maxWidth:'1440px',
  width: '100%',
  height: props.ht,
  padding: '1rem',
  blockquote: {
    padding: '0',
    margin: '0',
    pre : {
      textAlign: 'center',
      fontSize: '2rem',
      lineHeight: '3.5rem',
      margin: '0',
    },
    p: {
      marginTop: '1rem',
      textAlign: 'center',
      fontFamily:'Volkorn, serif',
      fontSize: '1.8rem',
      lineHeight: '1rem',
    }
  }
}))

const QuoteImage = styled ('div') ({
  position: 'absolute',
  width: '77px',
  height: '65px',
  background: `url(${QuoteImg})`,
  backgroundRepeat: 'no-repeat',
})

class Quote extends React.Component {
  render () {
    let pos = this.props.quotesPos;
    return (
      <Container>
        <UserImage>
          <img src={this.props.userImg} alt={this.props.alt}/>
        </UserImage>
        <QuoteImage style={{left:pos.x1, top: pos.y1,  zIndex: '200'}}/>
        <InnerQuote ht={this.props.ht}>
          <blockquote style={{paddingLeft:this.props.offset}}><pre style={{fontSize:this.props.fontSize}}>{this.props.copy}</pre><p>-{this.props.cite}</p></blockquote>
        </InnerQuote>
        <QuoteImage style={{left: pos.x2, top: pos.y2, zIndex: '300', transform:'scaleX(-1) scaleY(-1'}}/>
      </Container>
    )
  }
}

export default Quote;