import React from 'react';
import styled from '@emotion/styled';

const Container = styled('div')`
  width: 100%;
  background-color: ${props => (props.theme.colors.lightBlue)};
  border-top: 1px solid ${props => (props.theme.colors.medBorder)};
  margin-top: 2rem;
  & h6 {
    padding: 20px 0 60px 2.5rem;
    font-weight: 500;
  }
`

class Footer extends React.Component {
  render () {
    return (
      <Container>
        <h6>© Zameer Rehmani 2023</h6>
      </Container>
    )
  }
}

export default Footer;