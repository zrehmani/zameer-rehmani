import React from 'react';
import {Player, BigPlayButton} from 'video-react'
import styled from '@emotion/styled'
import "../../node_modules/video-react/dist/video-react.css"; // import css

const Container = styled('div') `
  & div :focus { outline: none; }
  & .video-react .video-react-poster {
    background-color: #FFFFFF;
  }
  & .video-react .video-react-big-play-button.video-react-big-play-button-center.video-react-big-play-button-round {
      margin-top: -1em;
      margin-left: -1em;
      border-radius: 50%;
      line-height: 1.9;
      width: 2em;
      height: 2em;
      font-size: 3rem;
  }
`

class VideoPlayer extends React.Component {
  render() {
    return (
      <Container>
        <Player
          ref='player'
          playsInline
          poster={this.props.poster}
          src={this.props.src}
        >
        <BigPlayButton position={'center'} className='video-react-big-play-button-round'/>
        </Player>
      </Container>
    )
  }
}

export default VideoPlayer;