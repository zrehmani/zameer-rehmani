/** @jsx jsx */
import { css, jsx } from '@emotion/react'
import React from 'react'
import { Link } from 'gatsby';
import styled from "@emotion/styled";

const List = styled('ol')`

  font-size: 1.125rem;
  font-weight: 500;
  list-style: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  
  
  & li {
    display: inline-block;
    margin: 0 2rem 0 0;
    & :last-child {
      margin: 0;
    }
  }
  
  & a {
    color: ${props => (props.theme.colors.primaryText)};
    text-decoration: none;
    & :hover, :active {
      color: ${props => (props.theme.colors.primary)};
    }
  }
  
  & a.active {
    color: ${props => (props.theme.colors.primary)};
  }
  
  @media (max-width: 840px) {
    font-size: 1.5rem;
    margin-top: 1rem;
  }
  
  // @media (min-width: 600px) and (max-width: 720px) {
  //   font-size: 1.5rem;
  // }
  //
  // @media (min-width: 720px) and (max-width: 840px) {
  //     font-size: 1.5rem;
  // }
    
  @media (min-width: 840px) and (max-width: 960px) {
     font-size: 1.4rem;
  }
  
  @media (min-width: 960px) and (max-width: 1280px) {
     font-size: 1.25rem;
  }
`

const NavLink = (props) => {
  return (
    <Link
      {...props}
      getProps={({ isPartiallyCurrent }) => {
        // the object returned here is passed to the
        // anchor element's props
        return {
          // style: isPartiallyCurrent ? {color:"darkorange"} : {}
          className: isPartiallyCurrent ? 'active': ''
        };
      }}
    />
  )
}

class Nav extends React.Component {
  render () {
    return (
      <List>
        <li><NavLink to="/portfolio/">portfolio</NavLink></li>
        <li><NavLink to="/about/">about</NavLink></li>
        <li><a href='/Zameer_Rehmani_Resume-lts.pdf' target='blank'>resumé</a></li>
      </List>
    )
  }
}

export default Nav


