import React from 'react'
import styled from "@emotion/styled";
import { Link } from 'gatsby';

const Container = styled('div') `
  margin-top: 4rem;
  div.enabled:hover {
    background-color: ${props => (props.theme.colors.lightBlue)};
    box-shadow: 1px 2px 8px rgba(189, 189, 189, 1);
    border: 1px solid ${props => (props.theme.colors.medBorder)};
  }
`
const Item = styled('div') `
  border: 1px solid ${props => (props.theme.colors.border)};
  width: 1024px;
  height: 455px;
  margin: 0 auto;
  margin-bottom: 4rem;
  background-color: ${props => (props.theme.colors.white)};
  box-shadow: 1px 2px 4px rgba(189, 189, 189, .5);
  
  & .pr-image {
    float: left;
    width: 399px;
    height: 455px;
  }
  
  & .pr-details {
    // background: azure;
    float: right;
    width: 623px;    
    height: 455px;
    padding-right: 56px;
      
    & h4 {
      text-align: right;
      margin-top: 40px;
      font-size: 0.875rem;
      color: ${props => (props.theme.colors.lightText)};
      font-weight: 400;
    }
    
    & h2 {
      font-size: 1.5rem;
      font-weight: 500;
      margin-bottom: 4px;
      color: ${props => (props.theme.colors.primaryText)};
    }
    
    & h3 {
      font-size: 1.0625rem;
      font-weight: 400;
      font-style: italic;
      margin-bottom: 2rem;
      color: ${props => (props.theme.colors.lightText)};
    }
    
    & div {
      border-left: 1px solid ${props => (props.theme.colors.border)};
      padding-top: 27px;
      padding-left: 57px;
      margin-top: 72px;
      & p {
        padding-bottom: 27px;    
        font-size: 1.3125rem;
        color: ${props => (props.theme.colors.primaryText)};
      }
    }
  }
  
  & :after {
    content: "";
    display: table;
    clear: both;
  }
`

class Preview extends React.Component {
  render () {

    let i = 0;

    let items = this.props.data.map((item) => {
      let enabled = !(typeof item.enabled !== 'undefined' && !item.enabled);
      let children = (
        <div>
          <div className='pr-image'>
            <img src={item.image} alt={item.title}/>
          </div>
          <div className='pr-details'>
            <h4>{item.tags.join(', ')}</h4>
            <div>
              {!enabled && <h2 style={{fontWeight:'700', marginBottom: '1rem'}}>COMING SOON!</h2>}
              <h2>{item.title}</h2>
              <h3>{item.type}</h3>
              {enabled && <p>{item.details}</p>}
            </div>
          </div>
        </div>
      )
      return (
        <Item key={i++} className={enabled ? 'enabled' : ''}>
          {enabled ? (
            <Link to={item.link}>
              {children}
            </Link>
          ) : (
            <div>
              {children}
            </div>
          )}
        </Item>
      )
    })

    return (
      <Container>{items}</Container>
    )
  }
}

export default Preview;