import React from 'react'
import { useForm, ValidationError } from "@formspree/react";
import styled from "@emotion/styled";

const Form = styled ('form') `
  width: 100%;
  //max-width: 32rem;
  margin: auto;
  font-size: 1rem; // 18px, usually
  line-height: 1.5; // Unitless! 1.5x the font size
  font-family: inherit;
  //border-top: 1px solid #9B9B9B;
  
  * {
    display: block;
  }
  
  fieldset {
    border: none;
    margin: .5rem 0;
    //padding: .5rem 0;
    //background-color: #FCFCFC;
  }
  
  input, textarea {
    width: 100%;
    color: #4A4A4A;
  }
  
  textarea {
    height: 200px;
  }
  
  button, textarea, input {
    padding: 0.5rem;
    border: 1px solid lightgray;
  }
  
  input {
    top-margin: 1rem;
  }
  
  label {
    font-weight: 500;
    padding: .4rem 0;
  }
  
  button {
    width: 150px;
  }
  
  *:focus {
    outline: 2px solid lightskyblue;
    outline-offset: 2px;
  }
  
  .error {
    color: #E91E63;
    padding: 0.5rem 0;
  }
  
  //.error:first-of-type {
  // margin-top:-1.5rem;
  //}

`

export default function ContactForm () {
    const [state, handleSubmit] = useForm("moqzleeo");

    if (state.succeeded) {
        return <p style={{fontWeight: 'bold'}}>Thank you! I will be in touch.</p>;
    }

    return (
        <Form onSubmit={handleSubmit}>
            <fieldset>
                <label htmlFor="email">Your Email Address</label>
                <input id="email" type="email" name="email" placeholder="username@example.com"/>
                <div className='error'><ValidationError prefix="Email" field="email" errors={state.errors} /></div>
            </fieldset>
            <fieldset>
                <label htmlFor="message">Message</label>
                <textarea id="message" name="message" />
                <div className='error'><ValidationError prefix="Message" field="message" errors={state.errors}/></div>
            </fieldset>
            <button type="submit" disabled={state.submitting}>
                Submit
            </button>
            <div className='error'><ValidationError errors={state.errors} /></div>
        </Form>
    );
}