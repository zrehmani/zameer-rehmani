import React from 'react'
import { Link } from 'gatsby'
import LogoImage from '../images/logo.png';
import Nav from '../components/nav';

/** @jsx jsx */
import { jsx } from '@emotion/react'
import styled from "@emotion/styled";

const Container = styled('header')`

  // padding: 7rem 5rem 1.3rem 5rem;
  // background: #FCFCFC;
  height: 80px;
  max-width: 1440px;
  margin: 0 auto;
  
  & .logo-container {
    float: left;
    margin-left: 2.5rem;
    & h1 {
      margin: 0;
      padding: 0;
      max-width: 372px;
      font-weight:normal;
      height: 0;
      display: block;
    }
    & a {
      text-decoration: none;
    }
    & .logo {
      width: 218px;
      height: 21px;
      margin-top: 35px;
      display: inline-block;
      background-image: url(${LogoImage});
      background-position: 0 0;
      background-repeat: no-repeat;
      background-size: 100% 100%;
    }
    & u {
      clip: rect(1px, 1px, 1px, 1px);
      height: 1px;
      left: auto;
      overflow: hidden;
      position: absolute;
      white-space: nowrap;
      width: 1px;
    }
  }
  
  & .nav-container {
    float: right;
    margin-top: 32px;
    margin-right: 2.5rem;
  }
  
  
  
  // @media (max-width: 840px) {
  //
  //   padding: 4rem 3rem 3rem 3rem;
  //
  //   & .logo-container {
  //     float: none;
  //     width: 100%;
  //   }
  //   h1 {
  //     margin: 0 auto;
  //   }
  //   & .nav-container {
  //     float: none;
  //     margin: 0;
  //     text-align: center;
  //   }
  // }
  //
  // @media (max-width: 720px) {
  //     padding: 4rem 3rem 2rem 3rem;
  //   }
`

class Header extends React.Component {

  render () {
    return (
      <Container>
        <div className={'logo-container'}>
          <h1>
            <a href="/" title="Zameer Rehmani">
              <i className="logo">
                <u>Zameer Rehmani Logo</u>
              </i>
            </a>
          </h1>
        </div>
        <div className={'nav-container'}>
          <Nav />
        </div>
        <div className={'clear'} />
      </Container>
    );
  }
}

export default Header