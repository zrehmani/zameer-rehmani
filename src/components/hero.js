import React from 'react';
import styled from "@emotion/styled";

const ImgContainer = styled('div') ({
  // borderTop: '1px solid rgba(220, 220, 220, 1)',
  // borderBottom: '1px solid rgba(220, 220, 220, 1)',
  lineHeight: '0',
  img : {
    margin: '0',

  }
})

const TextContainer = styled('div') ({
  backgroundColor: '#f4f9ff',
    ['-webkit-box-shadow']: "inset 0 -.1em 1.5em rgba(0, 0, 0, 0.1)",
    // BoxShadow: "inset 0 -10px 10px -10px rgba(0,0,0,0.5)",
  p : {
    width: '1000px',
    margin: "0px auto",
    padding: '80px 0px',
    whiteSpace: 'pre-wrap',
    fontFamily: "Nunito Sans, sans-serif",
    fontWeight: '700',
    fontSize: "2rem",
    //lineHeight: '3.7rem',
    // span: {
    //   color: '#eb1e63',
    // },
    a : {
      color: "inherit"
    },
    ["span > a"] : {
      color: '#eb1e63'
    },
    ["span:first-of-type"]:{
      color: '#eb1e63'
    },
    ["span:last-of-type"]:{
      fontSize:'1rem'
    }
  }
})

class Hero extends React.Component {
  render () {
    return !this.props.htmlText ? (
      <ImgContainer>
        <img src={this.props.image} alt={this.props.alt}/>
      </ImgContainer>
    ) : (
        <TextContainer>{this.props.htmlText}</TextContainer>
    )
  }
}

export default Hero;