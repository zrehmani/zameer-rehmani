import React from 'react';
import { Link } from 'gatsby';
import styled from "@emotion/styled";

const Container = styled ('p') (props => ({
  fontSize: '1rem',
  color: props.theme.colors.legacyText,
  a: {
    color: props.theme.colors.legacyText,
    '&:hover': {
      color: props.theme.colors.primary,
    }
  }

}))

class Breadcrumb extends React.Component {
  render () {
    return (
      <Container><span><Link to={"/"+this.props.section+"/"}>{this.props.section}</Link></span> / {this.props.page}</Container>
    )
  }
}

export default Breadcrumb;