import React from 'react';
import styled from "@emotion/styled";


const Container = styled ('div') `
    
    font-size: 1.125rem;
    line-height: 1.6;
    letter-spacing: -0.005em;
    text-rendering: optimizeLegibility;
    
    b {
      // font-weight: 600;
      color: ${props => (props.theme.colors.legacyText)};
    }
    
    h2 {
      font-weight: 500;
      font-size: 1.3rem;
      margin-bottom: 1rem;
    }
    
    h2.highlight {
      color: ${props => (props.theme.colors.primary)};
    }
    
    h4 {
      margin-bottom: 1.666rem;
      padding-bottom: 0.3rem;
      font-weight: 500;
      border-bottom: 1px solid ${props => (props.theme.colors.divider)};;
      color: ${props => (props.theme.colors.altText)};
     }
    
    .overview {
      margin-top: 8px;
      border-top: 1px solid ${props => (props.theme.colors.divider)};
      border-bottom: 1px solid ${props => (props.theme.colors.divider)};
      padding-top: 2rem;
      padding-bottom: 2rem;
      vertical-align: middle;
      font-size: 1.125rem;
      font-weight: 500;
      margin-bottom: 2rem;
      p {
        width: 550px;
        margin: 0 auto;
        line-height: 2.2;
      }
    }
    
    .sub > h3 {
      font-size: 1.125rem;
      font-weight: 500;
      border-bottom: 1px solid ${props => (props.theme.colors.divider)};    
      padding-bottom: .5rem;
      margin-top: 2rem;
      margin-bottom: 1rem;
    }
    
    ul {
      margin: 0 0 1rem 0;
      line-height: 1.5;
     
      li {
        font-size: 1rem;
        margin: 0 0 0 1rem;
      }
      h4 {
        font-size: 1rem;
        font-weight: 600;
        margin: 0 0 0.3rem 0;
      }
    }
    
    .insights {

      > div {
        display: inline-block;
        width: 162px;
        margin-right: 62px;
        margin-left: 10px;
        
        img {
          display:block;
          margin-bottom: 1rem;
        }
        
        p {
          // text-align: center;
          font-size: 1rem;
          // font-family: 'Avenir Next','Helvetica Neue','Segoe UI','Helvetica','Arial',sans-serif;
          color: ${props => (props.theme.colors.legacyText)};
          display:inline-block;
          vertical-align: top;
          width: 182px;
          // margin-right: 50px;
          line-height: 1.4;
          margin-left: -10px;
          // margin-top: 1rem;
          margin-bottom: 0;
        }
      }
    
      div:nth-child(3) {
        margin-right: 0;
      }
    }
    
    .persona-quote {
      
    //  padding: 1rem 0 1rem 0;
      // border-top: 1px solid ${props => (props.theme.colors.divider)};
      // border-bottom: 1px solid ${props => (props.theme.colors.divider)};
      margin-bottom: 1.666rem;
      
      img {
        width: 186px;
        display: inline-block;
        vertical-align: middle;
      }
      
      blockquote {
        display: inline-block;
        margin: 0;
        width: 444px;
        margin-left: 20px;
        vertical-align: middle;
        text-align: center;
        p {
          margin:0;
          font-family: 'Volkorn, sans-serif';
          font-size: 1.20rem;
          span {
            display: block;
            font-weight: 600;
          }
        }
      }
    }
    
    .problem-statement {
      
      padding-top: 1rem;
      padding-bottom: 1rem;
      
       border-top: 1px solid ${props => (props.theme.colors.divider)};
       border-bottom: 1px solid ${props => (props.theme.colors.divider)};
      
      > div {
        padding: 1.5rem 1.5rem 1.5rem 1.5rem;
        background-color: ${props => (props.theme.colors.lightBlue)};
        
        h3{
          font-size: 1.2rem;
          margin-bottom: 0.5rem;
          font-weight: 500;
        }
        p {
          margin: 0;
          font-weigth: 400;
        }
        
        p:nth-of-type(2) {
          padding-top: 1rem;
          font-weight: 500;
        }
      
      }
      
      
    }
    
    .annotations {
      
      margin-top: 2rem;
      
      div {
        float: left;
        vertical-align: top;
        margin-bottom: 2rem;
        img {
          width: 321px;
          float: left;
          vertical-align: top;
          margin: 0;
          padding:0;
          margin-left: -24px;
        }
      }
      
      div > div {
        width: 318px;
        margin-left: 1.5rem;
        
        h3 {
          font-size: 1.125rem;
          font-weight: 700;
          margin-bottom: 1rem;
          margin-left: 4rem;
        }
        
        ul {
        
          list-style: none;
        
          li {
          
            position: relative;
            left: 2rem;
            top: 0;
            margin: 0;
            margin-bottom: 1rem;
            
            > span {
              position: absolute;
              background-color: #4A90E2;
              width: 25px;
              height: 25px;
              border-radius: 50%;
              text-align: center;
              color: white;
              font-size: 1.15rem;
              font-weight: bold;
              line-height:1.3;
            }
            
            p {
              margin-left: 2.1rem;
              // font-family: 'Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
              font-size: 1rem;
              line-height: 1.5;
            }
            
          }
        }
      }
    }
    
    .tasks {
      ol {
        list-style: none;
        margin: 0;
        padding: 0;
        margin-bottom: 2rem;
        
        li {
          width: 100%;
          width: 382px;
          margin: 0 auto;
          margin-bottom: 0.5rem;
          p {
            display: inline-block;          
            margin: 0;
            padding: 0;
            margin-left: 1rem;
            margin-top: -8px;
            width: 306px;
            font-size: 1rem;
            line-height: 1.5;
            vertical-align: middle;
            font-family: 'Avenir Next', 'Helvetica Neue', 'Arial', sans-serif;
            font-weight: 500;
          }
          span {
            display: inline-block;        
            vertical-align: middle;
            img {
              padding: 0;
              margin: 0;
              width: 60px;
            }
          }
        }
      }
    }
    
    .usability-results {
      margin-bottom: 2rem;
    }
    
    .slides {
    
      > .slick-next {
        margin-top: -1rem;
        :before {
          color: ${props => (props.theme.colors.blue1)};
        }
      }
    
      > .slick-prev {
        margin-top: -1rem;
        :before {
          color: ${props => (props.theme.colors.blue1)};;
        }
      }
    }
    
    .slides2 {
    
      > .slick-next {
        // margin-top: -1rem;
        :before {
          color: ${props => (props.theme.colors.primaryText)};
        }
      }
    
      > .slick-prev {
        // margin-top: -1rem;
        :before {
          color: ${props => (props.theme.colors.primaryText)};;
        }
      }
    }
    
    .quotes {
    
      margin-bottom: 2rem;
      
      blockquote {
        
        display: inline-block;
        margin: 0;
        width: 650px;
        text-align: center;
        
        // border-top: 1px solid ${props => (props.theme.colors.divider)};
        // border-bottom: 1px solid ${props => (props.theme.colors.divider)};
        // padding-top: 1rem;
        // padding-bottom: 1rem;
        
        p {
          margin:0;
          background-color: ${props => (props.theme.colors.lightBlue)};     
          padding-top: 5rem;
          padding-bottom: 5rem;  
          font-family: 'Volkorn, sans-serif';
          font-weight: 500;
          font-size: 1.4rem;
        }
      }
    }
    
    .primary-list {
    
      ul {
        list-style: none;
        padding: 0;
        margin: 0;
        
        li {
          width: 618px;
          padding: 0;
          margin: 0;
          left: 2rem;
          position: relative;
          padding-bottom: 1rem;
          top: 0;
          // font-weight: 400;
          // color: ${props => (props.theme.colors.altText)};
          > span {
              position: absolute;
              left: -2rem;
              top: 5px;
              background-color: ${props => (props.theme.colors.primary)};
              width: 1rem;
              height: 1rem;
              border-radius: 50%;        
            }
          b {
            font-weight: 500;
          }
            
        }
      }
    
    }
    
    .zoom-image {
      position: relative;
      top: 0.5rem;
      left: 0;
      span {
        position: absolute;
        right: 0;
        top: -1rem;
        text-align: right;
        > img {
          width: 50%;
        }
      }
    
    }
    
    .process {
       h4 {
        border-bottom: 0;
        margin-bottom: 0;
        color: ${props => (props.theme.colors.primaryText)};
      }
      > ul {
        padding: 0 1rem;
        li {
          padding-left: 0;
        }
      }
    }
    
    img {
      margin: 0;
      padding: 0;
    }
    
    p {
      font-size: 1.125rem;
      line-height: 1.6;
      // margin-bottom: 1rem;
    }
    
    p.caption {
      font-size: 0.875rem;
      color: ${props => (props.theme.colors.secondaryText)};
      margin-top: -4px;
    }
    
    .caption2 {
      padding-top: 1rem;
      border-top: 1px solid black;
    }
    
    hr {
      margin: 0;
      margin-bottom: 0.5rem;
      background-color: ${props => (props.theme.colors.divider)};    
    }
    
    a {
      color: #676667;
      font-weight: 500;
    }
    
    a[href ^= 'mail'] {
      color: ${props => (props.theme.colors.primary)};
    }

`

const MainCopy = ({children}) => (
  <Container>
    {children}
  </Container>
)

export default MainCopy;