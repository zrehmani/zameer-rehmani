import React from 'react'
import { Link } from 'gatsby';
import styled from "@emotion/styled";

//data
const uxNav = ['UX Projects', 'navigation', ['Airbnb Discover', '/ux/discover/'], ['Nectar', '/ux/nectar/'], ['Spoitfy Shared Stream', '/ux/sharedstreams/'], ["Lowe's", '/ux/lowes/']]
const codeNav = ['Code Projects', 'navigation',
  ['Charles Schwab', '/code/charles-schwab/'],
  ["Clearasil", '/code/clearasil/'],
  ["Exxon Mobil", '/code/exxon-mobil/'],
  ["Kraft", '/code/kraft/'],
  ["NYSE", '/code/nyse/'],
  ['Volvo', '/code/volvo/'],
  ['Volvo S80', '/code/volvo-s80/'],
  ['Zac Posen', '/code/zac-posen/']]
  // ["DosXX", '/code/dosxx/'],
  // ["Zameer Rehmani", '/code/rehmani/']

const portfolio = ['Portfolio',
  'navigation',
  ['Airbnb Discover', '/portfolio/discover/'],
  ['Nectar', '/portfolio/nectar/'],
  // ['Spoitfy Shared Stream', '/portfolio/sharedstreams/'],
  ['Charles Schwab', '/portfolio/charles-schwab/'],
  ["Exxon Mobil", '/portfolio/exxon-mobil/'],
  ['Volvo', '/portfolio/volvo/'],
  ["Kraft", '/portfolio/kraft/'],
]
const contactNav = ['Contact', 'external', ['Email: zrehmani@gmail.com', 'mailto:zrehmani@gmail.com']]
const SideMenu = styled ('div') (props => ({
  fontSize: '1rem',
  // lineHeight: '1.5rem',
  // marginBottom: '3rem',
  marginTop: '4px',
  ul: {
    listStyle: 'none',
    margin: '0',
    paddingLeft: (props.dir === 'left') ? '5rem' : '1rem',
    li: {
      margin: 0,
      marginBottom: '0.2rem'
    },
    a: {
      display: 'inline-block',
      // color: 'rgba(125,125,125, 1)',
      color: props.theme.colors.legacyText,
      fontSize: 'normal',
      textDecoration: 'none',
      // padding: '5px',
      '&:hover': {
        // fontWeight: '500',
        color: props.theme.colors.primary
      }
    }
  },
  section: {
    marginBottom: '3rem'
  },
  h3: {
    fontSize: '1.125rem',
    fontWeight: '500',
    // color: props.theme.colors.secondaryText,
    // backgroundColor: 'rgba(240,245,250, 1)',
    // padding: ((props.dir === 'left') ? '0rem 0rem 0 05rem' : '0.5rem 1rem 0 1rem'),
    paddingLeft: '5rem',
    marginBottom: '0.5rem'
  }
}))

const active = {
  fontWeight:'500',
  color:'#E91E63',
  width: '100%',
};

const NavLink = (props) => {
  return (
    <Link
      {...props}
      getProps={({ isCurrent }) => {
        // the object returned here is passed to the
        // anchor element's props
        return {
          style: isCurrent ? active : {}
        };
      }}
    />
  )
};

let getNavData = (type) => {
  switch (type) {
    case SideNav.PORTFOLIO:
      return portfolio;
    case SideNav.UX:
      return uxNav;
    case SideNav.CODE:
      return codeNav;
    case SideNav.CONTACT:
      return contactNav;
    default:
      return uxNav;
  }
};

let getNavItems = (list, type) => {
  return list.map((list, i) => {
    let items;
    if (i > 1) {
      if (type === 'navigation') {
        items = <li key={i+1}><NavLink to={list[1]} activeStyle={active}>{list[0]}</NavLink></li>
      } else if (type === 'external') {
        items = <li key={i+1}><a href={list[1]}>{list[0]}</a></li>
      }
    }
    return items;
  });
}

class SideNav extends React.Component {
  render () {

    let nav = getNavData(this.props.type),
      direction = this.props.direction || 'left';

    return (
      <SideMenu dir={direction}>
        <section>
          <h3>{nav[0]}</h3>
          <ul>{getNavItems(nav, nav[1])}</ul>
        </section>
      </SideMenu>
    )
  }
}

SideNav.UX = 'ux';
SideNav.CODE = 'code';
SideNav.PORTFOLIO = 'portfolio';
SideNav.CONTACT = 'contact';
export default SideNav
