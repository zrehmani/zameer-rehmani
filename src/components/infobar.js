import React from 'react';
import styled from "@emotion/styled";

const List = styled('section') `
  
  font-size: 1rem;
  // line-height: 2rem;
  // margin-top: 8px;
  margin-bottom: 1.5rem;
  margin-top: 4px;
  
  
  & ul {
    color: ${props => (props.theme.colors.legacyText)};
    // padding-right: 2.5rem;
    list-style: none;
    margin: 0;
    // padding-left: 1rem;
    
    & li {
      // padding: 5px;
      margin: 0;
      margin-bottom: 0.2rem;
    }
  }
  
  & h3 {
    font-size: 1.125rem;
    font-weight: 500;
    // background-color: rgb(240, 245, 250);
    // padding: 0.5rem 6rem 0rem 1rem;
    // margin: 0 0 1rem 0;
    margin-bottom: 0.5rem;
  }
  
  & .primary {
    font-weight: 500;
  }
  
  & .sub {
    font-size: .875rem;
    font-style: italic;
    line-height: 1.3rem;
    margin-top: -15px;
  }
`

class Infobar extends React.Component {
  render () {
    let i=0;
    let items = this.props.items.map((item) => {

        if (!Array.isArray(item)) {
          return (
            <li key={i++}>
              <p>{item}</p>
            </li>
          )
        } else {
          return (
            <li key={i++}>
              <p>{item[0]}</p>
              <p className="sub">{item[1]}</p>
            </li>
            )
        }
      }
    )
    return (
      <List>
        <h3>{this.props.title}</h3>
        <ul>{items}</ul>
      </List>
    )
  }
}

export default Infobar;