import React from 'react'
import Layout from '../components/layout'
import Hero from '../components/hero'
import Preview from '../components/preview'
import {Link} from 'gatsby'
import Clients from '../components/clients'
// import BrandStatementImage from '../images/brand-statement2.jpg'
import NectarImage from '../images/preview/nectar.jpg'
import DiscoverImage from '../images/preview/discover.jpg'
import IBMImage from '../images/preview/ibm.jpg'
import VolvoImage from '../images/preview/volvo.jpg'
import SpotifyImage from '../images/preview/spotify.jpg'
import KraftImage from '../images/preview/kraft.jpg'
import SchwabImage from '../images/preview/schwab.jpg'
import ExxonImage from '../images/preview/exxon.jpg'
import {jsx} from "@emotion/react";

const data = [
  // {
  //   image: IBMImage,
  //   link: '/portfolio/archive_ibm/archive_ibm-projects/',
  //   tags: ['UX Research', 'UX/UI Design'],
  //   title: 'IBM Work',
  //   type: 'Latest IBM projects',
  //   details: 'Please request password to view this work.'
  // },
  {
    image: DiscoverImage,
    link: '/portfolio/discover/',
    tags: ['UX Research', 'UX/UI Design'],
    title: 'Airbnb Discover',
    type: 'iOS App Concept',
    details: 'A social app concept that helps user find travel recommendations they trust.'
  },{
    image: NectarImage,
    link: '/portfolio/nectar/',
    tags: ['UX Research', 'UX/UI Design'],
    title: 'Nectar Mattress',
    type: 'Mobile Web Redesign',
    details: 'Improve customer journey and flow to effectively raise key success metric of revenue per session.'
  },{
    image: VolvoImage,
    link: '/portfolio/volvo/',
    tags: ['Creative Development', 'Animation'],
    title: 'Volvo S80',
    type: 'Rich Media Microsite',
    details: 'Introducing new S80 Volvo in an engaging multimedia interactive campaign.'
  },{
    image: ExxonImage,
    link: '/portfolio/exxon-mobil/',
    tags: ['Creative Development', 'Animation'],
    title: 'Exxon Mobil',
    type: 'Rich Media Microsite',
    details: 'A multimedia site highlighting Exxon Mobil\'s technological, social, and environmental initiatives.'
  },{
    image: SchwabImage,
    link: '/portfolio/charles-schwab/',
    tags: ['Creative Development', 'Animation'],
    title: 'Charles Schwab',
    type: 'Rich Media Microsite',
    details: 'Talk to Chuck campaign encouraged users to share their thoughts on recession to win $10,000.'
  },{
    image: KraftImage,
    link: '/portfolio/kraft/',
    tags: ['Creative Development', 'Animation'],
    title: 'Kraft Holiday',
    type: 'Rich Media Microsite',
    details: 'An extensive interactive multimedia showcase including games, recipes, and promotions.'
  },{
    image: SpotifyImage,
    link: '/portfolio/sharedstreams/',
    tags: ['UX Research', 'UX/UI Design'],
    title: 'Spotify Shared Stream',
    type: 'App Feature Design',
    details: 'A new feature that helps user contribute their music to create and enjoy a shared experience.',
    enabled: false
  }
]
let brand_statement = <p>
  <span>Hello.</span>  I am Zameer Rehmani.  <br/>I am a designer and developer with over 20 years of <a href="/Zameer_Rehmani_Resume-lts.pdf" target="_blank">experience</a>.  <br/>Currently, I work as a Design Lead at <a href="https://www.ibm.com/automation" target="_blank">IBM Automation</a>.
  <br/>
  <span>Below are a few projects I have done over the years.  For private client work, please <Link to="/about#contact">contact</Link> me.</span>
</p>;

class Portfolio extends React.Component {
  render () {
    return (
      <Layout>
        <div>
          <Hero htmlText={brand_statement}/>
          <Preview data={data} />
          <div style={{width:'1024px', margin: '0 auto'}}>
            <Clients />
          </div>
        </div>
      </Layout>
    )
  }
}

export default Portfolio;