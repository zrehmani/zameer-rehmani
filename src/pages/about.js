import React from 'react'
import Layout from '../components/layout'
import Hero from '../components/hero'
import Infobar from '../components/infobar';
import MainCopy from '../components/maincopy';
// import SideNav from '../components/sidenav';
import ExternalLink from '../components/external-link'
import AboutImage from '../images/about-hero22.jpg'
import LinkedInImage from '../images/about/linked-in.png'
// import ClientsImage from '../images/about/clients.png'
//import Clients from '../components/clients';
import styled from '@emotion/styled';
import ContactForm from '../components/contact-form'

const AboutNav = styled('div')`
  & ul {
    margin: 36px 0 0 150px;
    padding: 0;
    list-style: none;
    font-size: 1rem;
    font-weight: 400;
    line-height: 2.5;
    & li {
      margin: 0;
      padding: 0;
    }
    & a {
      color: ${prop => (prop.theme.colors.primary)}
    }
  }
`
//data
const expertise = ['UX Research, UX/UI Design', 'Front End Development', 'Graphic Design', 'Creative Technology', 'Digital Advertising', 'Ad Technology', 'Team Management', ]
const education = [['PRATT INSTITUTE', 'MFA, Computer Graphics & Interactive Media'],['BOSTON UNIVERSITY', 'BFA, Graphic Design'],['GENERAL ASSEMBLY', 'Certificate, UX Design Immersive']]

const AboutPage = () => (
  <Layout>

    <div>

      <Hero image={AboutImage} alt='about hero' />

      <div className={'row'}>

        <div className={'col1'}>
          {/*<div style={{float:'left', marginLeft:'-5rem'}}>*/}
            {/*<SideNav type = {SideNav.PORTFOLIO}/>*/}
          {/*</div>*/}
          <AboutNav>
            <ul>
              <li><a href='/Zameer_Rehmani_Resume-lts.pdf' rel="noopener noreferrer" target='blank'>Resumé</a></li>
              <li><a href='https://www.linkedin.com/in/rehmani/' rel="noopener noreferrer" target='blank'>LinkedIn</a></li>
              <li><a href='mailto: zrehmani@gmail.com' target='_blank' rel="noopener noreferrer">Email</a></li>
            </ul>
          </AboutNav>
        </div>

        <div className={"col2"} style={{height:'100vh'}}>
          <MainCopy>
            {/*<p>I believe the user-centric approach based on human need and empathy is critical in designing successful and relevant digital products.  My years of experience in design and development lends me a holistic perspective to effectively collaborate on products that can make a genuine difference to users and businesses alike.</p>*/}
            <div style={{borderBottom:'1px solid lightgray'}}>
              <h2>ABOUT ME</h2>
              <p>I am a perpetual act of balancing dualities.  A pragmatic idealist.  A designer and a coder.  An introvert who enjoys social interactions.  A big picture thinker yet detail oriented.  My peers describe me as someone who brings high positive energy to the team that is natural and uplifting.  </p>
              <p>I  enjoy listening to classical music and frequent art museums in the city.  My favorite painters are Cézanne and Kandinsky and you can often find me sketching or painting my abstract musings.</p>
            </div>
            <div style={{marginTop:'2rem'}}>
              <h2 id={"contact"} >Contact</h2>
              <p style={{marginTop:'-.5rem', fontSize:'1rem', color: '#4A4A4A'}}>For work related inquiry, please fill the form below. </p>
              {/*at: <a href='mailto: zrehmani@gmail.com'>zrehmani@gmail.com</a></p>*/}
              <ContactForm/>
            </div>
          </MainCopy>
        </div>

        <div className={"col3"}>
          {/*<div style={{float:'right', marginRight:'-5rem', width:'100%'}}>*/}
            {/*<div style={{marginTop:'-60px'}}>*/}
              {/*<div style={{float:'left', marginLeft:'1rem'}}>*/}
                {/*<ExternalLink data={{*/}
                  {/*link: "http://www.rehmanidigital.com/Zameer_Rehmani_Resume-lts.pdf",*/}
                  {/*label: 'view resume'*/}
                {/*}}/>*/}
              {/*</div>*/}
              {/*<div style={{float:'left', paddingLeft:'16px'}}>|</div>*/}
              {/*<div style={{float:'left', paddingLeft:'16px', paddingTop:'4px'}}>*/}
                {/*<a href="https://www.linkedin.com/in/rehmani/" target="_blank" rel="noopener noreferrer" style={{textDecoration:"none", color: "#676667"}} >*/}
                  {/*<div>*/}
                    {/*<img src={LinkedInImage} alt="linked-in" width='75%'/>*/}
                  {/*</div>*/}
                {/*</a>*/}
              {/*</div>*/}
              {/*<div className={'clear'} />*/}
            {/*</div>*/}
            {/*<Infobar title="Expertise" items={expertise}/>*/}
            {/*<Infobar title="Education" items={education}/>*/}
            {/*<SideNav type={SideNav.CONTACT} direction="right"/>*/}
          </div>

        </div>

      </div>
      {/*<div>*/}
        {/*<div className={'row'} style={{paddingTop:'0'}}>*/}
          {/*<div className={'col1'}>&nbsp;</div>*/}
          {/*<div className={'col2'}>*/}
            {/*/!*<h3 style={{fontWeight:'500', fontSize:'1.5rem'}}>Clients</h3>*!/*/}
            {/*<div>*/}
              {/*<Clients />*/}
            {/*</div>*/}

          {/*</div>*/}
          {/*<div className={'col1'}>&nbsp;</div>*/}
          {/*<div className={'clear'} />*/}
        {/*</div>*/}
        {/*/!*<img src={ClientsImage} alt="" style={{margin:'0'}}/>*!/*/}

      {/*</div>*/}
  </Layout>
)

export default AboutPage