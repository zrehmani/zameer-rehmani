import React from 'react'
import Layout from '../components/layout'
import CodePreview from '../components/code/code-preview'
import VolvoS80Image from '../images/code/volvo-s80.png'
// import DosXXImage from '../images/code/dosxx.png'
import ClearasilImage from '../images/code/clearasil.png'
import ExxonMobilImage from '../images/code/exxon.png'
import KrafImage from '../images/code/kraft.png'
import VolvoLifeImage from '../images/code/volvo-for-life.png'
import ZacPosenImage from '../images/code/zacposen.png'
import CharlesSchwabImage from '../images/code/charles-schwab.png'
import NYSEImage from '../images/code/nyse.png'

//data
const data = [
  {
    link:'/code/volvo/',
    label:'Volvo',
    img:VolvoLifeImage,
  },
  {
    link:'/code/kraft/',
    label:'Kraft Ritz',
    img:KrafImage,
  },
  {
    link:'/code/exxon-mobil/',
    label:'Exxon Mobil Challenges',
    img:ExxonMobilImage,
  },
  {
    link:'/code/volvo-s80/',
    label:'Volvo S80',
    img:VolvoS80Image,
  },
  // {
  //   link:'/code/dosxx/',
  //   label:'Dos XX',
  //   img:DosXXImage,
  // },
  {
    link:'/code/clearasil/',
    label:'Clearasil Confidence',
    img:ClearasilImage,
  },


  {
    link:'/code/zac-posen/',
    label:'Zac Posen',
    img:ZacPosenImage,
  },
  {
    link:'/code/charles-schwab/',
    label:'Charles Schwab Contest',
    img:CharlesSchwabImage,
  },
  {
    link:'/code/nyse/',
    label:'NYSE',
    img:NYSEImage,
  }
  // ,
  // {
  //   link:'/code/rehmani/',
  //   label:'Zameer Rehmani',
  //   img:RehmaniImage,
  // }
]


const CodePage = () => (
  <Layout>
    <div className={'row'} style={{borderTop:'1px solid rgb(220, 220, 220)', backgroundColor:'rgb(244,249,250)', padding:'5rem 4rem 0 4rem'}}>
        <CodePreview data={data}/>
    </div>
  </Layout>
)

export default CodePage