import React from 'react';
import styled from "@emotion/styled";
import Layout from '../../components/layout'
import VideoPlayer from '../../components/video-player'
import Poster from '../../video/exxon-mobil.png'
import Video from '../../video/exxon-mobil.mp4';
import Breadcrumb from '../../components/breadcrumb'
import SideNav from '../../components/sidenav';
import MainCopy from '../../components/maincopy';
import ExternalLink from '../../components/external-link';
import Infobar from '../../components/infobar';
import Hero from '../../components/hero';
import HeroImage from '../../images/code/exxon-hero2.jpg';

class ExxonMobil extends React.Component {
  render () {
    return (
      <Layout>

        {/*<Hero>*/}
          {/*<VideoPlayer poster={Poster} src={Video} backgroundColor={'#FFFFFF'}/>*/}
        {/*</Hero>*/}

        <Hero image={HeroImage} alt='exxon hero'/>

        <div className={'row'} style={{paddingTop: '1rem'}}>
          <div className={'col1'}>
            <Breadcrumb section="portfolio" page="exxon-mobil"/>
          </div>
          <div className={'col2'}>&nbsp;</div>
          <div className={'col1'}>
            &nbsp;
            {/*<div style={{float:'right'}}>*/}
              {/*<ExternalLink data={{*/}
                {/*link: "http://www.rehmanidigital.com/clients/exxon-mobil/energy-challenges/demo/",*/}
                {/*label: 'view project (flash)'*/}
              {/*}}/>*/}
            {/*</div>*/}
            {/*<div className={"clear"} />*/}
          </div>
        </div>

        <div className={'row'} style={{paddingTop: '1rem'}}>
          <div className={'col1'}>
            <div style={{marginLeft:'-0.5rem', marginTop: '2px'}}>
              <SideNav type = {SideNav.PORTFOLIO}/>
            </div>
          </div>
          <div className={"col2"} style={{height:'100vh'}}>
          <MainCopy>
              <h2>Project Overview</h2>
              <p>As a part of integrated media campaign, we launched an Exxon Mobil site highlighting its technological, environmental and social initiatives "take on the world's toughest energy challenges."</p>
              <p>I designed the architecture for this site to be driven completely by XML.  Any content - including an entire page and menu and videos with keyframes can be added simply through XML without having to recompile the code base.</p>
              <h4 style={{marginBottom:'1rem'}}>PROJECT VIDEO</h4>
              <VideoPlayer poster={Poster} src={Video}/>
              <hr style={{marginTop:'1rem'}}/>
            </MainCopy>
          </div>

          <div className={'col1'}>
            <div style={{marginTop:'2px'}}>
              <Infobar title="Client/Agency" items={['Exxon Mobil / Havas']}/>
              <Infobar title="Roles" items={['Tech Lead', 'Creative Developer']}/>
              <Infobar title="Technical" items={['ActionScript', 'Flash Builder / Eclipse', 'PixLib Framework', 'Ant', 'Flash', 'Photoshop', 'Illustrator', 'HTML', 'JavaScript', 'XML']}/>
            </div>
          </div>
        </div>

      </Layout>
    )
  }
}

export default ExxonMobil