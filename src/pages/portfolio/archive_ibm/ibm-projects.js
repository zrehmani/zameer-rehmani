import React from 'react';
// import {graphql} from 'gatsby';
// import Img from 'gatsby-image';
import Slider from 'react-slick';
import ImageZoom from 'react-medium-image-zoom';
import Layout from '../../../components/layout'
import DiscoverHero from '../../../images/portfolio/discover/hero-discover2.jpg';
import AffinityImage from '../../../images/portfolio/discover/affinity-map-1.jpg';
import AffinityZoomImage from '../../../images/portfolio/discover/affinity-map-1-zoom.jpg';
// import InsightsImage from '../../images/portfolio/discover/insights-percentage.jpg';
import Insights80Image from '../../../images/portfolio/discover/insight-80.jpg'
import Insights60Image from '../../../images/portfolio/discover/insight-60.jpg'
import Insights100Image from '../../../images/portfolio/discover/insight-100.jpg'
import AmyImage from '../../../images/portfolio/discover/amy-persona.jpg';
import FeatureImage from '../../../images/portfolio/discover/research-design.jpg';
import FeatureZoomImage from '../../../images/portfolio/discover/research-design-zoom.jpg';
import WireframeImage from '../../../images/portfolio/discover/sketches.jpg';
import WireframeZoomImage from '../../../images/portfolio/discover/sketches-zoom.jpg';
import Discover1Image from '../../../images/portfolio/discover/discover-1.jpg';
import Discover2Image from '../../../images/portfolio/discover/discover-2.jpg';
import Discover3Image from '../../../images/portfolio/discover/discover-3.jpg';
import Discover4Image from '../../../images/portfolio/discover/discover-4.jpg';
import Num1Image from '../../../images/portfolio/discover/num1.jpg';
import Num2Image from '../../../images/portfolio/discover/num2.jpg';
import Num3Image from '../../../images/portfolio/discover/num3.jpg';
import Num4Image from '../../../images/portfolio/discover/num4.jpg';
import WireframeFinalImage from '../../../images/portfolio/discover/wireframes-final.jpg';
import UsabilityResultsImage from '../../../images/portfolio/discover/usability-result.jpg';
// import DesignMap from '../../images/ux/discover/design-map.png';
// import DesignBookMark1 from '../../images/ux/discover/design-bookmark-1.png';
// import DesignBookMark2 from '../../images/ux/discover/design-bookmark-2.png';
// import DesignCheckIn from '../../images/ux/discover/design-checkin.png';
// import DiscoverLogo from '../../images/ux/discover/discover-logo.png';
import SideNav from '../../../components/sidenav';
import Breadcrumb from '../../../components/breadcrumb';
import Infobar from '../../../components/infobar';
import Hero from '../../../components/hero';
// import MainCopy from '../../components/maincopy';
// import Quote from '../../components/ux/quote';
// import ProblemStatement from '../../components/ux/problem-statement';
// import Annotation from '../../components/ux/annotation';
// import BigPrototypeLink from '../../components/ux/big-prototype-link'
import MainCopy from '../../../components/maincopy';
import ExternalLink from '../../../components/external-link'
// import styled from '@emotion/styled';

let team = ['Natasha Jacchan', 'Meghan Salviejo', 'Zameer Rehmani'];
let uxMethods = ['User Interviews', 'Affinity Mapping', 'Persona Development', 'Design Studio', 'Wireframing', 'Rapid Prototype', 'Usability Testing', 'Specification Documentation'];

const settings = {
    dots: false,
    arrows: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 5000,
    infinite: true,
    className:'slides'
}

const IBMProjects = (props) => (
    <Layout>

        <div>

            <Hero image={DiscoverHero} alt='airbnb discover hero'/>

            <div className={'row'} style={{paddingTop: '1rem'}}>
                <div className={'col1'}>
                    <Breadcrumb section="portfolio" page="airbnb-discover"/>
                </div>
                <div className={'col2'}>&nbsp;</div>
                <div className={'col1'}>
                    <div style={{float:'right'}}>
                        <ExternalLink data={{
                            link: "https://invis.io/MYPAZJ9VDF7",
                            label: 'view prototype'
                        }}/>
                    </div>
                    <div className={"clear"} />
                </div>
            </div>

            <div className={'row'} style={{paddingTop: '1rem'}}>
                <div className={'col1'}>
                    <div style={{marginLeft:'-0.5rem', marginTop: '2px'}}>
                        <SideNav type = {SideNav.PORTFOLIO}/>
                    </div>
                </div>
                <div className={"col2"}>

                    <MainCopy>
                        <div className={'overview'}>
                            <p>Airbnb Discover is a standalone social app concept for Airbnb platform.  Through the app, users can explore what their friends are doing around the world.  Users can save these activities, customize them and share their own experiences.</p>
                        </div>
                        {/*<hr/>*/}
                        <h2>RESEARCH & SYNTHESIS</h2>
                        <p>We initially started out with an assumption that Airbnb users could use a virtual conceirge app to help them create a more personalized experience.  However, our user research pointed to a different problem altogether.</p>

                        <ImageZoom zoomImg={{src:AffinityZoomImage}}>
                            <img src={AffinityImage} alt='Discover Affinity Map'/>
                        </ImageZoom>

                        <p className={'caption'}>Affinity Mapping to identify patterns and form insights</p>
                        <div className={'sub'}><h3>RESEARCH PROCESS</h3></div>
                        <div className={'process'}>
                            <ul>
                                <h4>User Interviews</h4>
                                <li>2 head concierges to understand how to best serve travellers</li>
                                <li>5 users from Airbnb user demographic (millenialls, budget savy) who travelled one or more times in the past year.</li>
                            </ul>
                            <ul>
                                <h4>Affinity Mapping</h4>
                                <li>Synthesized data from user interviews to find patterns and form insights</li>
                            </ul>
                            <ul>
                                <h4>Persona Development</h4>
                                <li>Created a persona of the primary user - Amy by highlighting behaviors, goals, needs, and pain points to guide through the iterative design phase</li>
                            </ul>
                        </div>


                        <div className={'sub'}><h3>INSIGHTS: A PROBLEM OF TRUST</h3></div>

                        <p>Three key insights from our research showed that rather than a virtual concierge (initial assumption), users really needed an <b>easy way to access travel recommendations from friends because they trusted them the most.</b> Based on this discovery, we redefined the Problem Statement and created a Persona (Amy) to guide us through the design phase.</p>
                        <div className={'insights'} style={{marginBottom: '1.666rem'}}>
                            <div>
                                <img src={Insights80Image} alt="Discover Insight 80"/>
                                <p>of users get <b>information fatigue</b> while researching travel plans online with over 20+ sources</p>
                            </div>
                            <div>
                                <img src={Insights60Image} alt="Discover Insight 60"/>
                                <p>of users are concerned about the <b>credibility</b> of online travel resources</p>
                            </div>
                            <div>
                                <img src={Insights100Image} alt="Discover Insight 100"/>
                                <p>of users depend on <b>friends</b> for #1 source of travel recommendations</p>
                            </div>
                        </div>
                        <hr style={{marginBottom:'1.5rem'}}/>
                        <div className={'persona-quote'}>
                            <img src={AmyImage} alt="Amy Persona"/>
                            <blockquote><p>Friends always give the best recommendations.<br/>
                                I trust them and they know me and what I like.<span>-Amy</span></p></blockquote>
                        </div>

                        <div className={'problem-statement'} style={{marginBottom: '2rem'}}>
                            <div>
                                <h3>PROBLEM STATEMENT</h3>
                                <p>Amy is overwhelmed when choosing travel destinations/activities online because she doesn’t trust all of the sources. She has to reach out to her friends for recommendations because she trusts them.</p>
                                <p>How might we make travel recommendations from Amy’s friends more easily accessible to her?</p>
                            </div>
                        </div>
                        <h2>DESIGN: IDEATION & ITERATION</h2>
                        <p>In the design phase, we set out to create the Most Viable Product (MVP) that effectively addressed the problem discovered in the research phase.</p>

                        <ImageZoom zoomImg={{src:FeatureZoomImage}}>
                            <img src={FeatureImage} alt="Discover Feature" className='img'/>
                        </ImageZoom>

                        <p className={'caption'}>Whiteboarding with team to determine features based on research</p>
                        <div className={'sub'}><h3>DESIGN PROCESS</h3></div>
                        <div className={'process'}>
                            <ul>
                                <h4>Design Studios (2 Rounds)</h4>
                                <li>Ideated through quick sketches for effective user interface and interactions</li>
                            </ul>
                            <ul>
                                <h4>Feature Prioritization</h4>
                                <li>White-boarded as team to determine features based on research</li>
                                <li>Created priority matrix to assist in feature prioritizations</li>
                            </ul>
                            <ul>
                                <h4>Wireframe & User Flows</h4>
                                <li>Built low-fi to high-fi wireframes and user flows</li>
                            </ul>
                            <ul>
                                <h4>Prototyping</h4>
                                <li>Built prototypes from wireframes to set up usability testing</li>
                            </ul>
                            <ul>
                                <h4>Usability Testing (2 Rounds)</h4>
                                <li>Formal Usability Testing with 5 users per round to validate design decisions</li>
                            </ul>
                        </div>

                        <ImageZoom zoomImg={{src:WireframeZoomImage}}>
                            <img src={WireframeImage} alt="Wireframe sketches" className='img' style={{border: '1px solid #444', background:'#444'}}/>
                        </ImageZoom>

                        <p className={'caption'}>Design Studio 2: Low Fidelity Wireframe Sketches </p>
                        <div className={'sub'}><h3>RESEARCH TO DESIGN</h3></div>
                        <p>Every feature of the app was deliberated and designed to help Amy access her friends’ recommendations easily.  In our design studios, we created visual metaphors like an <b>interactive map</b> that can quickly show Amy where her friends are around the world.  A <b>social feed</b>, on the other hand will show her friends’ recent activities which she can <b>bookmark</b> to save.  And finally she can <b>checkout</b> a location and share her recommendations with her friends.</p>
                        <div className={'annotations'}>
                            <div>
                                <img src={Discover1Image} alt="Discover Annotations 1"/>
                                <div>
                                    <h3>Map & Social Feed</h3>
                                    <ul>
                                        <li><span>1</span><p>With a map, Amy can quickly see where her friends are. She can also interact with it by zooming in/out, panning and clicking.</p></li>
                                        <li><span>2</span><p>Amy can change the context of her map by changing location and time range.</p></li>
                                        <li><span>3</span><p>Social feed shows Amy what her friends are doing now.  She can like the post and bookmark it from the feed itself.</p></li>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                <img src={Discover2Image} alt="Discover Annotations 2"/>
                                <div>
                                    <h3>Bookmarking</h3>
                                    <ul>
                                        <li><span>1</span><p>Amy can bookmark an activity from the activity page.</p></li>
                                        <li><span>2</span><p>Amy can easily access all her saved activities via global bookmark navigation in the tab bar.</p></li>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                <img src={Discover3Image} alt="Discover Annotations 3"/>
                                <div>
                                    <h3>Bookmark Features</h3>
                                    <ul>
                                        <li><span>1</span><p>Amy can build and share her own custom trip from saved bookmarks.</p></li>
                                        <li><span>2</span><p>Amy can filter, search or see her bookmarks on a map.</p></li>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                <img src={Discover4Image} alt="Discover Annotations 4"/>
                                <div>
                                    <h3>Checking-in</h3>
                                    <ul>
                                        <li><span>1</span><p>Amy can check-in to an activity via global tab bar.</p></li>
                                        <li><span>2</span><p>She can write in to describe her experience.</p></li>
                                        <li><span>3</span><p>She can give the activity a rating.</p></li>
                                        <li><span>4</span><p>She can also post pictures.</p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className={'clear'} />
                        <div className={'sub'}><h3>HI-FIDELITY WIREFRAMES FROM FINAL PROTOTYPE</h3></div>
                    </MainCopy>
                </div>
                <div className={"col1"}>
                    <div style={{marginTop:'2px'}}>
                        <Infobar title="UX Methods" items={uxMethods}/>
                        <Infobar title="Team" items={team}/>
                        <Infobar title="My Roles" items={['UX Researcher','UX/UI Design']}/>
                        <Infobar title="Tools" items={['Sketch', 'InVision', 'Adobe CS']}/>
                        <Infobar title="Timing" items={['2 Weeks Sprint']}/>
                    </div>
                </div>
                <div>
                    <img src={WireframeFinalImage} alt="Discover - All Final Wireframes"/>
                </div>
            </div>
            <div>
                <div className={'row'} style={{paddingTop: '1rem'}}>
                    <div className={'col1'}>&nbsp;</div>
                    <div className={'col2'}>
                        <MainCopy>
                            <div className={'sub'}><h3>USABILITY TESTING</h3></div>
                            <p>Two rounds of usability tests were conducted on 5 users to validate design decisions.  Users were selected from Airbnb user demographic - millenials, budget-savy consumers who had travelled one or more times in the last year.</p>
                            <p>We asked users to complete four tasks to test functionality of the home screen, bookmarking and checking into an activity.</p>
                            <div className={'tasks'}>
                                <h4>TASKS</h4>
                                <ol>
                                    <li><span><img src={Num1Image} alt="Num 1"/></span><p>Explore the app to find out more about what your friends have done recently.</p></li>
                                    <li><span><img src={Num2Image} alt="Num 2"/></span><p>View an activity to learn more about it, and bookmark it for future reference.</p></li>
                                    <li><span><img src={Num3Image} alt="Num 3"/></span><p>Go check out your saved bookmarks.</p></li>
                                    <li><span><img src={Num4Image} alt="Num 4"/></span><p>Now you are doing an activity, use the app to check into that activity.</p></li>
                                </ol>
                            </div>

                            <div className={'usability-results'}>
                                <h4>TEST RESULTS ON FINAL PROTOTYPE</h4>
                                <img src={UsabilityResultsImage} alt="Usability Results"/>
                            </div>

                            <h4>USER QUOTES ON FINAL PROTOTYPE</h4>

                            <Slider {...settings}>
                                <div className={'quotes'}>
                                    <blockquote><p>It’s good for people like me<br/>who have wanderlust.</p></blockquote>
                                </div>
                                <div className={'quotes'}>
                                    <blockquote><p>I love that I can see what other people are doing.<br/>And it's also nice to know what to do in the city where I live.</p></blockquote>
                                </div>
                                <div className={'quotes'}>
                                    <blockquote><p>I didn't realize until this interview, <br/> how often I do typically ask friends for recommendations...</p></blockquote>
                                </div>
                                <div className={'quotes'}>
                                    <blockquote><p>There's a lot of space around the information <br/> so it's easy to see what's important. And it's familiar.</p></blockquote>
                                </div>
                            </Slider>



                            <h2>KEY TAKEAWAY</h2>
                            <p>One major takeaway was to see <b>how critical is user research to product design</b> - our assumption about virtual concierge seemed like a great idea at first but <b>listening to users and empathizing with their real challenges revealed a far more interesting problem</b>.  We realized that more information is not necessarily a good thing because it can be contradictory and confusing and lead to information fatigue.  Users trust those who know them and in the often impersonal web of limitless information - <b>users still yearn for connection and trust to navigate and make decisions on the web</b>.  Applications that address this human need are far more likely to be relevant and successful.</p>
                        </MainCopy>
                    </div>
                    <div className={'col1'}>&nbsp;</div>
                </div>
            </div>
        </div>
    </Layout>
)

// export const imageQuery = graphql`
//   query {
//     discoverHero: file(relativePath: { eq: "portfolio/discover/hero-discover@2x.jpg" }) {
//       childImageSharp {
//         fixed(width: 1440) {
//           ...GatsbyImageSharpFixed
//         }
//       }
//     }
//   }
// `

export default IBMProjects