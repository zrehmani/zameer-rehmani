import React from 'react';
// import styled from "@emotion/styled";
import Layout from '../../components/layout'
import VideoPlayer from '../../components/video-player'
import Poster from '../../video/volvo.png'
import Video from '../../video/volvo.mp4';
import Breadcrumb from '../../components/breadcrumb'
import SideNav from '../../components/sidenav';
import MainCopy from '../../components/maincopy';
import ExternalLink from '../../components/external-link';
import Infobar from '../../components/infobar';
import HeroImage from '../../images/code/volvo-hero2.jpg';
import Hero from '../../components/hero';
// const Hero = styled('div') `
//   border-top: 1px solid rgb(220,220,220);
//   border-bottom: 1px solid rgb(220,220,220);
// `

class Volvo extends React.Component {
  render () {
    return (
      <Layout>

        {/*<Hero>*/}
          {/*<VideoPlayer poster={Poster} src={Video}/>*/}
        {/*</Hero>*/}

        <Hero image={HeroImage} alt='volvo hero'/>

        <div className={'row'} style={{paddingTop: '1rem'}}>

          <div className={'col1'}>
            <Breadcrumb section='portfolio' page='volvo'/>
          </div>

          <div className={'col2'}>&nbsp;</div>

          <div className={'col1'}>
            {/*<div style={{float:'right'}}>*/}
              {/*<ExternalLink data={{*/}
                {/*link: "http://www.rehmanidigital.com/clients/volvo/s80-2008/demo/",*/}
                {/*label: 'view project'*/}
              {/*}}/>*/}
            {/*</div>*/}

            {/*<div className={"clear"} />*/}
          </div>

        </div>

        <div className={'row'} style={{paddingTop: '1rem'}}>
          <div className={'col1'}>
            <div style={{marginLeft:'-0.5rem', marginTop: '2px'}}>
              <SideNav type = {SideNav.PORTFOLIO}/>
            </div>
          </div>
          <div className={"col2"} style={{height:'100vh'}}>
          <MainCopy>
              <h2>Project Overview</h2>
              <p>An immersive experience of Volvo S80 through a 360 degree interactive video highlighting all new luxury features.</p>
              <p>As a tech lead, I introduced MVC (model-view-controller) Framework and built our first site using modular workflow in the rich media team at Havas.  This experiment was so successful that this process was adopted for all big rich media projects.  The biggest technical challenge in the project was to load the main video in parts so the site can load quickly and then stitch the parts so the video seamlessly plays as one piece.  I wrote the main code base, site architecture, animations, video player code and managed the team.</p>
              <h4 style={{marginBottom:'1rem'}}>PROJECT VIDEO</h4>
              <VideoPlayer poster={Poster} src={Video}/>
              <hr style={{marginTop:'1rem'}}/>
            </MainCopy>
          </div>

          <div className={'col1'}>
            <div style={{marginTop:'2px'}}>
              <Infobar title="Client/Agency" items={['Volvo / Havas']}/>
              <Infobar title="Roles" items={['Tech Lead', 'Creative Developer']}/>
              <Infobar title="Technical" items={['ActionScript', 'PixLib Framework', 'Flash Builder', 'Ant', 'Flash', 'Photoshop', 'Illustrator', 'HTML', 'JavaScript', 'XML']}/>
            </div>
          </div>

          <div className={"clear"}/>
        </div>
      </Layout>
    )
  }
}

export default Volvo