import React from 'react';
import styled from "@emotion/styled";
import Layout from '../../components/layout'
import VideoPlayer from '../../components/video-player'
import Poster from '../../video/charles-schwab.png'
import Video from '../../video/charles-schwab.mp4';
import Breadcrumb from '../../components/breadcrumb'
import SideNav from '../../components/sidenav';
import MainCopy from '../../components/maincopy';
import ExternalLink from '../../components/external-link';
import Infobar from '../../components/infobar';
import HeroImage from '../../images/code/schwab-hero2.jpg';
import Hero from '../../components/hero';
// const Hero = styled('div') `
//   border-top: 1px solid rgb(220,220,220);
//   border-bottom: 1px solid rgb(220,220,220);
// `

class CharlesSchwab extends React.Component {
  render () {
    return (
      <Layout>

        {/*<Hero>*/}

        {/*</Hero>*/}

        <Hero image={HeroImage} alt='charles schwab hero'/>

        <div className={'row'} style={{paddingTop: '1rem'}}>
          <div className={'col1'}>
            <Breadcrumb section="portfolio" page="charles-schwab"/>
          </div>

          <div className={'col2'}>&nbsp;</div>

          <div className={'col1'}>&nbsp;
            {/*<div style={{float:'right'}}>*/}
              {/*<ExternalLink data={{*/}
                {/*link: "http://www.rehmanidigital.com/clients/charles-schwab/talktochuck/demo/",*/}
                {/*label: 'view project'*/}
              {/*}}/>*/}
            {/*</div>*/}

            {/*<div className={"clear"} />*/}
          </div>
        </div>

        <div className={'row'} style={{paddingTop: '1rem'}}>
          <div className={'col1'}>
            <div style={{marginLeft:'-0.5rem', marginTop: '2px'}}>
              <SideNav type = {SideNav.PORTFOLIO}/>
            </div>
          </div>
          <div className={"col2"} style={{height:'100vh'}}>
            <MainCopy>
              <h2>Project Overview</h2>
              <p>Talk to Chuck was an online contest for users to submit a quote with an opportunity to win $10,000 worth of a Schwab investment account. It highlighted consumer concerns in a difficult economy and encouraged them to take concrete actions.</p>
              <p>As a creative developer, I designed and developed all the animations and front end code.  I also provided technical guidance and support to the cross-functional campaign team.</p>
              <h4 style={{marginBottom:'1rem'}}>PROJECT VIDEO</h4>
              <VideoPlayer poster={Poster} src={Video}/>
              <hr style={{marginTop:'1rem'}}/>
            </MainCopy>
          </div>

          <div className={'col1'}>
            <div style={{marginTop:'2px'}}>
              <Infobar title="Client/Agency" items={['Charles Schwab / Havas']}/>
              <Infobar title="Roles" items={['Creative Developer']}/>
              <Infobar title="Technical" items={['ActionScript', 'LowRA Framework', 'Flash Builder', 'Ant', 'Flash', 'Photoshop', 'Illustrator', 'HTML', 'JavaScript', 'XML']}/>
            </div>
          </div>

        </div>
      </Layout>
    )
  }
}

export default CharlesSchwab