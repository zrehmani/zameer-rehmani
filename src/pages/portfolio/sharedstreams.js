import React from 'react';

//images
import HeroImage from '../../images/ux/sharedstream/hero.png';
import JonImage from '../../images/ux/sharedstream/jon.png';
import JoinImage1 from '../../images/ux/sharedstream/join-1.png';
import JoinImage2 from '../../images/ux/sharedstream/join-2.png';
import CreateImage1 from '../../images/ux/sharedstream/create-1.png';
import CreateImage2 from '../../images/ux/sharedstream/create-2.png';
import CreateImage3 from '../../images/ux/sharedstream/create-3.png';
import CreateImage4 from '../../images/ux/sharedstream/create-4.png';
import SharedStreamLogo from '../../images/ux/sharedstream/sharedstream-logo.png';

//components
import Layout from '../../components/layout'
import SideNav from '../../components/sidenav';
import Breadcrumb from '../../components/breadcrumb';
import Infobar from '../../components/infobar';
import Hero from '../../components/hero';
import MainCopy from '../../components/maincopy';
import Quote from '../../components/ux/quote';
import ProblemStatement from '../../components/ux/problem-statement';
import Annotation from '../../components/ux/annotation';
import BigPrototypeLink from '../../components/ux/big-prototype-link'
import ExternalLink from '../../components/external-link'

//data
let team = ['Daniel Fançois', 'Melissa Frank', 'Jacob Thomas Davidson', 'Zameer Rehmani'];
let uxMethods = ['Screener Survey', 'User Interview', 'Affinity Mapping', 'Persona', 'Problem Statement', 'Usability Testing', 'Design Studio', 'Wireframes', 'Prototype', 'Wireflow'];
let quote1 ="People take turns because only one person can play music at a time."
let quote2 = "The live feature is pretty cool - I feel like I am \n" +
  "listening to a live radio stream."
let problemStatement = "Jon avoids contributing to the music being played at a party because he feels he cannot do so unobtrusively."
let problemOpportunity = "How can we enable him to seamlessly integrate his music into the environment?"
let annotation1 = {
  title: 'Join Shared Stream',
  list: [
    "Jon can easily find and join current live colloborative streams under Shared Streams section on his home page.",
    "Jon will be able to click JOIN to start listening to the live Shared Stream.",
    "If he has permission from the stream creator, Jon can add songs to the queue.",
    "And if allowed by stream creator, Jon can also invite his friends to the stream."
  ]
}
let annotation2 = {
  title: 'New Shared Stream',
  list: [
    "Jon can initiate a Shared Stream from his library via “Your Library” button in global tab bar.",
    "Jon can give his Shared Stream a title.",
    "Jon can also add friends he wants to colloborate with.",
    "Jon can apply permission levels for his stream which will affect who can see the stream and who can contribute songs to it and whether anyone can requests songs to be added to the queue."
  ]
}
let annotation3 = {
  // title: 'Checking-in',
  list: [
    "Jon can hit “Go Live” button from his Shared Stream interface to start broadcasting his Shared Stream.",
    "He can add songs to the queue.",
    "He can add his friends as colloborators.",
    "Jon can end his Shared Stream when he is done broadcasting his stream."
  ]
}

let prototypeLink= 'https://projects.invisionapp.com/share/7KNH2Q0VMNJ#/screens/313162299_Home_Screen';

const SharedStream = () => (
  <Layout>
    <div>

      <Hero image={HeroImage} alt='spotify shared stream hero'/>

      <div className={'row'}>
        <div className={'col1'}>
          <Breadcrumb section="portfolio" page="spotify-shared-stream"/>
          <div style={{float:'left', marginLeft:'-5rem'}}>
            <SideNav type = {SideNav.PORTFOLIO}/>
          </div>
        </div>
        <div className={"col2"}>
          <MainCopy>
            <h2>Project Overview</h2>
            <p>Spotify Shared Stream adds a new interactive feature to Spotify mobile app that allow users to collaborate together to create a DJ set or a playlist in real time.</p>
            <h2>User Research</h2>
            <p style={{marginBottom:'2rem'}}>To start out, our big question was whether the product goal of adding this collaborative feature to the Spotify app aligned with the user needs?  To find out, we reached out to general audience via Screener Survey to find the right target audience for a music app.  From 38 respondents, we selected 5 for our User Interviews and discovered three major user insights that supported the idea of colloborative music:</p>
            <ol style={{fontSize:'1.5rem', lineHeight:'3rem'}}>
              <li>Users are generally private with music, but it’s still important for them to share and colloborate with friends.</li>
              <li>Users enjoy discovering new music as long as its easy and accessible.</li>
              <li>Music is common element in social gatherings and there is a general interest in participating/influencing the ambiance.</li>
            </ol>
            <p>Based on these insights, we built a User Persona of Jon and defined a Problem Statement to guide us through the design phase. Jon likes to share his music but is frustrated that he doesn’t have enough control over the music being played at social gatherings.  He has to take turn to play music with friends and wishes there was an easier way for him to contribute his music. Lastly, Jon enjoys finding new music and artists.</p>
          </MainCopy>
        </div>
        <div className={"col1"}>
          <div style={{float:'right', marginRight:'-5rem', width:'100%'}}>
            <div style={{float:'left', marginLeft:'1rem', marginTop:'-60px'}}>
              <ExternalLink data={{
                link: prototypeLink,
                label: 'view prototype'
              }}/>
            </div>
            <Infobar title="Team" items={team}/>
            <Infobar title="UX Methods" items={uxMethods}/>
          </div>
        </div>
        <div className={"clear"}/>
      </div>

      <Quote copy={quote1} cite="Jon" userImg={JonImage} ht={"140px"} offset={'250px'} fontSize={"1.875rem"} quotesPos={{x1:'355px', y1:'10px', x2:'1280px', y2:'10px'}}/>

      <div style={{marginTop:'155px'}}>
        <ProblemStatement statement={problemStatement} opportunity={problemOpportunity}/>
      </div>

      <div className={"row"}  >
        <div className={"col1"}><p/></div>
        <div className={"col2"} >
          <MainCopy>
            <h2>Research To Design</h2>
            <p>We conducted a Design Studio to brainstorm and sketch ideas  in a team to come up with user interface ideas for this new feature.  We also had to ensure that the user interface seamlessly integrated with the current Spotify aesthetics and its dark theme.</p>
            <p>Two major activities Jon would engage in with regards to the new colloborative music functionality is to join a Shared Stream in progress or start one himself.  Both these user flows are addressed in our design solutions below.</p>
          </MainCopy>
        </div>
        <div className={"col1"}><p/></div>
        <div className={"clear"} />
      </div>

      <div className={"row"}  style={{marginTop:'-4rem'}}>
        <div className={"col1"}><p/></div>
        <div className={"col2"}  style={{textAlign:'center'}}>
          <img style={{marginLeft:'-23rem'}} src={JoinImage1} alt=""/>
          <img src={JoinImage2} style={{paddingLeft:"50px"}} alt=""/>
        </div>
        <div className={"col1"}>
          <Annotation data={annotation1} />
        </div>
        <div className={"clear"} />
      </div>

      <div className={"row"} >
        <div className={"col1"}><p/></div>
        <div className={"col2"}  style={{textAlign:'center'}}>
          <img style={{marginLeft:'-23rem'}} src={CreateImage1} alt=""/>
          <img src={CreateImage2} style={{paddingLeft:"50px"}} alt=""/>
        </div>
        <div className={"col1"}>
          <Annotation data={annotation2} />
        </div>
        <div className={"clear"} />
      </div>

      <div className={"row"} >
        <div className={"col1"}><p/></div>
        <div className={"col2"}  style={{textAlign:'center'}}>
          <img style={{marginLeft:'-23rem'}} src={CreateImage3} alt=""/>
          <img src={CreateImage4} style={{paddingLeft:"50px"}} alt=""/>
        </div>
        <div className={"col1"}>
          <Annotation data={annotation3} offset={4}/>
        </div>
        <div className={"clear"} />
      </div>

      <div style={{marginTop:'140px'}}>
        <Quote copy={quote2} cite="Jon" userImg={JonImage} ht={"200px"} offset={"80px"} quotesPos={{x1:'375px', y1:'-30px', x2:'1000px', y2:'70px'}}/>
      </div>

      <div className={"row"}  style={{marginTop: '340px'}}>
        <div className={"col1"}><p/></div>
        <div className={"col2"} >
          <MainCopy>
            <h2>Key Takeaway</h2>
            <p>We conducted three rounds of usability tests with five users and with each round, we realized we had to make it easier and easier for users to notice the new feature.</p>
            <p>For example, we need to be mindful of the terminology being used because users may not understand or relate to it.  Initially, we called the new feature, DJ Set rather than Shared Stream but users felt that the feature is only for professional DJs and they were confused by the DJ’ing terms such as set (collection of songs in the queue).</p>
            <p>To alleviate this misunderstanding, we conducted A/B test with new terminologies like Shared Stream and Live Stream and we discovered that users found both of them more intuitive.  We eventually decided with Shared Stream because it better encompasses the live and colloborative nature of this feature.</p>
            <p>Another consideration was to keep the fine balance between advertising the new feature while staying within the constraints of Spotify’s aesthetics and functionality.  For example, to have the new feature stand out in user Library - we decided to use the word “new” in green Spotify color next to the feature list.  This helped users identify the “new” feature better.</p>
            <p>Even with all these helpful improvements, we concluded that a relevant on-boarding experience that shows the user how to use and enjoy this feature would be a necessary .  It will ensure that user not only finds the feature easily but is able to take advantage of it immediately.</p>
          </MainCopy>
        </div>
        <div className={"col1"}><p/></div>
        <div className={"clear"} />
      </div>

      <div className={"row"}  style={{marginTop:'-4rem', paddingBottom: '4rem'}}>
        <BigPrototypeLink img={SharedStreamLogo} link={prototypeLink}/>
      </div>

    </div>
  </Layout>
)

export default SharedStream