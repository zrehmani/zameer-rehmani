
//library
import React from 'react';
import Slider from 'react-slick';
import ImageZoom from 'react-medium-image-zoom';

//components
import Layout from '../../components/layout'
import SideNav from '../../components/sidenav';
import Breadcrumb from '../../components/breadcrumb';
import Infobar from '../../components/infobar';
import Hero from '../../components/hero';
// import MainCopy from '../../components/maincopy';
// import Quote from '../../components/ux/quote';
// import ProblemStatement from '../../components/ux/problem-statement';
// import Annotation from '../../components/ux/annotation';
// import BigPrototypeLink from '../../components/ux/big-prototype-link'
import MainCopy from '../../components/maincopy';
import ExternalLink from '../../components/external-link'

//assets
import HeroImage from '../../images/portfolio/nectar/nectar-hero.jpg';
import AffinityImage from '../../images/portfolio/nectar/affinity.jpg';
import CompetitorImage from '../../images/portfolio/nectar/competitive.jpg';
import ClickToZoomImage from '../../images/click-zoom.png';
import AbbyImage from '../../images/portfolio/nectar/abby.jpg';
import LikertImage from '../../images/portfolio/nectar/likert.jpg';
import UserJourneyImage from '../../images/portfolio/nectar/user-journey.jpg';
import UserJourneyZoomImage from '../../images/portfolio/nectar/user-journey-zoom.jpg';
import CardSort1Image from '../../images/portfolio/nectar/card-sort1.jpg';
import SiteMapImage from '../../images/portfolio/nectar/site-map.jpg';
import NavImage from '../../images/portfolio/nectar/navigation-design.jpg';
import ProposedLayoutImage from '../../images/portfolio/nectar/proposed-layout.jpg';
import Insights80Image from '../../images/portfolio/discover/insight-80.jpg'
import Insights100Image from '../../images/portfolio/discover/insight-100.jpg'
import PersonaImage from '../../images/portfolio/nectar/persona.jpg';
import CheckoutFlowCurrentImage from '../../images/portfolio/nectar/checkout-flow-current.jpg';
import CheckoutFlowProposedImage from '../../images/portfolio/nectar/checkout-flow-proposed.jpg';
import Prototype from '../../images/portfolio/nectar/nectar-prototype.gif';
import LikertPrototypeImage from '../../images/portfolio/nectar/likert-prototype.jpg';


// import FeatureImage from '../../images/portfolio/discover/research-design.jpg';
// import FeatureZoomImage from '../../images/portfolio/discover/research-design-zoom.jpg';
// import WireframeImage from '../../images/portfolio/discover/sketches.jpg';
// import WireframeZoomImage from '../../images/portfolio/discover/sketches-zoom.jpg';
// import Discover1Image from '../../images/portfolio/discover/discover-1.jpg';
// import Discover2Image from '../../images/portfolio/discover/discover-2.jpg';
// import Discover3Image from '../../images/portfolio/discover/discover-3.jpg';
// import Discover4Image from '../../images/portfolio/discover/discover-4.jpg';
import Num1Image from '../../images/portfolio/discover/num1.jpg';
import Num2Image from '../../images/portfolio/discover/num2.jpg';
import Num3Image from '../../images/portfolio/discover/num3.jpg';
// import Num4Image from '../../images/portfolio/discover/num4.jpg';
// import WireframeFinalImage from '../../images/portfolio/discover/wireframes-final.jpg';
// import UsabilityResultsImage from '../../images/portfolio/discover/usability-result.jpg';


let team = ['Dhwani Krishnan', 'Kevin Regan', 'Zameer Rehmani'];
let uxMethods = ['Business Research', 'User Interviews', 'Affinity Mapping', 'Competitive Feature Analysis', 'Persona Development', 'Heuristic Analysis', 'Site Map', 'User Journey Map', 'Design Studio (with client team)', 'Wireframing', 'Rapid Prototype', 'Usability Testing', 'Specification Documentation'];

// const settings = {
//   dots: false,
//   arrows: true,
//   speed: 500,
//   fade: true,
//   cssEase: 'linear',
//   autoplay: true,
//   autoplaySpeed: 5000,
//   infinite: true,
//   className:'slides'
// }

const Nectar = (props) => (
  <Layout>

    <div>
      <Hero image={HeroImage} alt='airbnb discover hero'/>
      <div className={'row'} style={{paddingTop: '1rem'}}>
        <div className={'col1'}>
          <Breadcrumb section="portfolio" page="nectar"/>
        </div>
        <div className={'col2'}>&nbsp;</div>
        <div className={'col1'}>
          <div style={{float:'right'}}>
            <ExternalLink data={{
              link: "https://invis.io/REO81FEM567",
              label: 'view prototype'
            }}/>
          </div>
          <div className={"clear"} />
        </div>
      </div>

      <div className={'row'} style={{paddingTop: '1rem'}}>
        <div className={'col1'}>
          <div style={{marginLeft:'-0.5rem', marginTop: '2px'}}>
            <SideNav type = {SideNav.PORTFOLIO}/>
          </div>
        </div>
        <div className={"col2"}>

          <MainCopy>
            <div className={'overview'}>
              <p>Nectar is the third largest online mattress company.  We provided them with a mobile web redesign proposal that improves site’s usability and effectively raises online sales and brand value.</p>
            </div>
            {/*<hr/>*/}
            <h2>RESEARCH & SYNTHESIS</h2>
            <p>First, we set out to collect all relevant data through various research methods.  We <b>interviewed Nectar’s user base</b> to learn about their needs from an online store.  We <b>studied online mattress industry</b> and Nectar’s competition in the field.  And we analyzed and <b>tested Nectar’s site for usability</b> issues and <b>compared its performance and features</b> with its closest rivals in the industry.</p>


            {/*<img src={AffinityImage} alt=""/>*/}

            {/*<Img fixed={props.data.affinityMap.childImageSharp.fixed} />*/}

            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: AffinityImage,*/}
            {/*    alt:'Affinity Map',*/}
            {/*    className: 'img'*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: AffinityImage,*/}
            {/*    alt:'Affinity Map',*/}
            {/*    className: 'img'*/}
            {/*  }}*/}
            {/*/>*/}

            <ImageZoom>
              <img src={AffinityImage} alt='Affinity Map' className='img'/>
            </ImageZoom>

            <p className={'caption'}>Finding patterns and insights through Affinity Mapping</p>
            <div className={'sub'}><h3>USER INTERVIEWS</h3></div>
            <p>Interviewed 5 users from Nectar’s target user base - urban area millennial with extra money at their disposal.  We learned that most users <b>like free shipping and return on bulk items</b>.  They  prefer to <b>buy items on mobile app as opposed to mobile web browser</b>. And they are <b>frustrated by lack of space and the constant need to scroll on website</b>. </p>
            <h4>KEY INSIGHTS</h4>
            {/*<ul>*/}
              {/*<h4>User Interviews</h4>*/}
              {/*<li>2 head concierges to understand how to best serve travellers</li>*/}
              {/*<li>5 users from Airbnb user demographic (millenialls, budget savy) who travelled one or more times in the past year.</li>*/}
            {/*</ul>*/}
            {/*<ul>*/}
              {/*<h4>Affinity Mapping</h4>*/}
              {/*<li>Synthesized data from user interviews to find patterns and form insights</li>*/}
            {/*</ul>*/}
            {/*<ul>*/}
              {/*<h4>Persona Development</h4>*/}
              {/*<li>Created a persona of the primary user (Amy) by highlighting behaviors, goals, needs, and pain points to guide through the iterative design phase</li>*/}
            {/*</ul>*/}

            {/*<div className={'sub'}><h3>INSIGHTS: A PROBLEM OF TRUST</h3></div>*/}
            {/*<p>Three key insights from our research showed that rather than a virtual concierge (initial assumption), users really needed an <b>easy way to access travel recommendations from friends because they trusted them the most.</b> Based on this discovery, we redefined the Problem Statement and created a Persona (Amy) to guide us through the design phase.</p>*/}
            <div className={'insights'} style={{marginBottom: '1.666rem'}}>
              <div>
                <img src={Insights80Image} alt="Insight 80"/>
                <p>of users buy bulky items online if they get <b>free shipping & return</b></p>
              </div>
              <div>
                <img src={Insights100Image} alt="Insight 100"/>
                <p>of users like <b>organized information without distractions</b> on websites</p>
              </div>
              <div>
                <img src={Insights100Image} alt="Insight 100"/>
                <p>of users <b>don't purchase on mobile web browser</b> (prefer mobile app)</p>
              </div>
            </div>
            <h4>USER QUOTE</h4>
            <div className={'persona-quote'}>
              <img src={PersonaImage} alt="Persona"/>
              <blockquote><p>If they have an app, I use that.<br/>App seems more user friendly.. more secure.<span>-Mady</span></p></blockquote>
            </div>
            <div className={'sub'}><h3>COMPETITIVE FEATURE ANALYSIS</h3></div>
            <p>We compared mobile website features between Nectar and its top two competitors - Casper and Purple Mattress.  We found that Nectar is missing some important features as compared to its rivals.</p>
            <h4>KEY INSIGHTS</h4>
            <div className={'primary-list'} style={{marginBottom: '1rem'}}>
              <ul>
                <li><span/><b>NAVIGATION</b> - All Nectar products are not available in the global navigation but only accessible through the footer.</li>
                <li><span/><b>LAYOUT</b> - Nectar home page requires 35% more scrolling.  Content is not designed for the mobile space.</li>
                <li><span/><b>CHECKOUT</b> - Nectar lacks a cart interface to display purchased items.  Users are taken straight to the chekcout page which is confusing.</li>
              </ul>
            </div>
            <h4>Nectar Home Page requires 35% more scrolling than Casper and Purple</h4>
            <div className={'zoom-image'}>
              <span> <img src={ClickToZoomImage} alt="click zoom button"/></span>
              {/*<ImageZoom*/}
              {/*  image={{*/}
              {/*    src: CompetitorImage,*/}
              {/*    alt:'Layout Competitor',*/}
              {/*    style:{minWidth: '740px', marginLeft: '-90px'}*/}
              {/*  }}*/}
              {/*  zoomImage={{*/}
              {/*    src: CompetitorImage,*/}
              {/*    alt:'Layout Competitor',*/}
              {/*    style:{width:'1000px'}*/}
              {/*  }}*/}
              {/*/>*/}
              <ImageZoom>
                <img src={CompetitorImage} alt='Layout Competitor' style={{minWidth:'740px', marginLeft: '-90px', marginTop:'10px'}}/>
              </ImageZoom>
            </div>
            <div className={'sub'}><h3>HEURISTIC ANALYSIS</h3></div>
            <p>We conducted heuristic analysis with the Abby Method (10 heuristic Information Architect principles) to test the usability of the current Nectar mobile website.</p>
            <h4>ABBY METHOD</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: AbbyImage,*/}
            {/*    alt:'Abby Method',*/}
            {/*    style: {marginBottom: '1.666rem'}*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: AbbyImage,*/}
            {/*    alt:'Abby Method',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={AbbyImage} alt='Abby Method' style={{marginBottom: '1.666rem'}} />
            </ImageZoom>
            <h4>KEY INSIGHTS</h4>
            <div className={'primary-list'} style={{marginBottom: '1rem'}}>
              <ul>
                <li><span/><b>NAVIGATION</b> is poorly organized.  Redesign primary navigation to include all the products to improve Findability and Accessibility.</li>
                <li><span/><b>LAYOUT</b> could be better organized.  Simplify design and organization to to remove redundancy and unnecessary scrolling.</li>
                <li><span/><b>CHECKOUT</b> flow is confusing without a shopping cart.  Redesign the flow with a shopping cart to avoid confusion and frustration.</li>
              </ul>
            </div>
            <div className={'sub'}><h3>USABILITY TESTING (CURRENT SITE)</h3></div>
            <p>Conducted live usability testing via <a href='http://www.usertesting.com' target='blank'>usertesting.com</a> with 5 users for both desktop and mobile site for Nectar, Casper and Purple.</p>
            <h4>USER TASKS</h4>
            <div className={'tasks'}>
              <ol>
                <li><span><img src={Num1Image} alt="Num 1"/></span><p>You are looking to purchase a pillow. So, find one and add it to the cart.</p></li>
                <li><span><img src={Num2Image} alt="Num 2"/></span><p>You are looking for a mattress and not sure whether you need a California King or a King. Find out about both.</p></li>
                <li><span><img src={Num3Image} alt="Num 3"/></span><p>Shop for a mattress and a foundation and add to your cart.</p></li>
              </ol>
            </div>
            <h4>KEY INSIGHTS</h4>
            <div className={'insights'} style={{marginBottom: '1.666rem'}}>
              <div>
                <img src={Insights80Image} alt="Insight 80"/>
                <p>of users said they felt being <b>forced to checkout (no cart)</b></p>
              </div>
              <div>
                <img src={Insights80Image} alt="Insight 80"/>
                <p>of users said <b>content was poorly organized</b> (repetitive info, scrolling)</p>
              </div>
              <div>
                <img src={Insights100Image} alt="Insight 100"/>
                <p>of users felt <b>navigation is not clear</b> - does not include all the products.</p>
              </div>
            </div>
            <h4>USER QUOTES</h4>
            <Slider {...{
                      dots: false,
                      arrows: true,
                      speed: 500,
                      fade: true,
                      cssEase: 'linear',
                      autoplay: true,
                      autoplaySpeed: 5000,
                      infinite: true,
                      className:'slides'
                    }}>
              <div className={'quotes'}>
                <blockquote><p>Honestly, at this point, <br/>I would assume they don’t sell pillows.</p></blockquote>
              </div>
              <div className={'quotes'}>
                <blockquote><p>This is a lot of scrolling<br/> for a website.</p></blockquote>
              </div>
              <div className={'quotes'}>
                <blockquote><p>Wait, why am I at checkout already? <br/>I still need to add my pillows.</p></blockquote>
              </div>
              <div className={'quotes'}>
                <blockquote><p>I don’t know how to find out more <br/>info which is a little frustrating.</p></blockquote>
              </div>
            </Slider>
            <h4>LIKERT SCALES</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: LikertImage,*/}
            {/*    alt:'Likert Scale Current',*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: LikertImage,*/}
            {/*    alt:'Likert Scale Current',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={LikertImage} alt='Likert Scale Current'/>
            </ImageZoom>
            <div className={'sub'}><h3>USER JOURNEY MAP</h3></div>
            <p>Based on the persona and the usability test results, we created a user journey map that highlights Mady’s experience while interacting with the Nectar website.  It illustrates both <b>highs and lows of her journey</b> as well as the opportunities to <b>address her pain points</b>. </p>
            <p><b>Scenario:</b> Mady is a nurse at the Ohio Hospital who usually works 80 hours a week.  She is in the market for a new mattress and wants to purchase it online; however, she doesn't know what she really needs in her mattress. So far, she's been frustrated with finding all the information she needs for the right mattress.</p>
            <div className={'zoom-image'}>
              <span> <img src={ClickToZoomImage} alt="click zoom button" /></span>
              {/*<ImageZoom*/}
              {/*  image={{*/}
              {/*    src: UserJourneyImage,*/}
              {/*    alt:'User Journey Map',*/}
              {/*    style:{minWidth: '682px', marginLeft: '-32px', marginTop: '1rem', marginBottom: '2rem'}*/}
              {/*  }}*/}
              {/*  zoomImage={{*/}
              {/*    src: UserJourneyZoomImage,*/}
              {/*    alt:'User Journey Map',*/}
              {/*  }}*/}
              {/*/>*/}
              <ImageZoom zoomImg={{src:UserJourneyZoomImage}}>
                <img src={UserJourneyImage} alt='User Journey Map' style={{minWidth: '682px', marginLeft: '-32px', marginTop: '1rem', marginBottom: '2rem'}}/>
              </ImageZoom>
            </div>
            <div className={'problem-statement'} style={{marginBottom: '2rem'}}>
              <div>
                <h3>PROBLEM STATEMENT</h3>
                <p>Mady has a difficult time finding all information needed to purchase a mattress on Nectarsleep.com.</p>
                <p>How might we redesign the website so Mady can easily find information needed to decide and purchase various products on the website?</p>
              </div>
            </div>
            <h2>DESIGN: IDEATION & ITERATION</h2>
            <p>To help Mady easily find information to shop on Nectar’s website, our research clearly points to several key categories.  Redesign <b>primary navigation</b> so Mady can access all products easily.  Design a <b>mobile appropriate layout</b> by removing redundancies and unnecessary elements so Mady doesn’t have to scroll a lot to find information.  And finally, <b>improve the checkout flow by adding a cart overlay</b> when user adds items to a cart.  This allows them to continue adding other products and not feel forced to checkout.</p>
            {/*<img src={FeatureImage} alt="Discover Feature Prioritization"/>*/}


            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: CardSort1Image,*/}
            {/*    alt:'Card Sort Image',*/}
            {/*    className: 'img',*/}
            {/*    style: {border: '1px solid #444', background:'#444'}*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: CardSort1Image,*/}
            {/*    alt:'Card Sort Image',*/}
            {/*  }}*/}
            {/*/>*/}

            <ImageZoom>
              <img src={CardSort1Image} alt='Card Sort Image' className='img' style={{border: '1px solid #444', background:'#444'}} />
            </ImageZoom>

            <p className={'caption'}>Card sorting helps to see how the user would organize products in navigation</p>

            <div className={'sub'}><h3>GLOBAL NAVIGATION: INTUITIVE AND ACCESSIBLE</h3></div>
            <p>We redesigned global navigation so that Mady can easily access all products.  We added products accessible only in the footer to the global navigation.  And we conducted an <b>open card sorting</b> with 5 users to understand user mental model of products and their categories.  We used data and insights from the card sorting to help us <b>design a navigation that would be intuitive for Mady.</b></p>

            <h4>PROPOSED SITE MAP</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: SiteMapImage,*/}
            {/*    alt:'Site Map Image',*/}
            {/*    className: 'img',*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: SiteMapImage,*/}
            {/*    alt:'Site Map Image',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={SiteMapImage} alt='Site Map Image' className='img'/>
            </ImageZoom>

            <h4>PROPOSED NAVIGATION DESIGN</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: NavImage,*/}
            {/*    alt:'Nav Image',*/}
            {/*    className: 'img',*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: NavImage,*/}
            {/*    alt:'Nav Image',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={NavImage} alt='Nav Image' className='img'/>
            </ImageZoom>
            <p className={'caption'}>Current Global Navigation<span style={{marginLeft:'100px'}}>New Global Navigation with all products and new categories</span></p>


            <div className={'sub'}><h3>LAYOUT: DESIGN FOR MOBILE SPACE</h3></div>
            <p>We redesigned content and layout of the home and product pages to <b>help Mady quickly get to the most important content</b> she needs to make her decisions.  We removed all unnecessary white space and decorative elements. We made the image dimensions more appropriate for mobile so they don’t take too much space.  We <b>removed all redundant content</b> and consolidated them under clear logical categories.  We added background photos and color for <b>clear division of content</b> so Mady can quickly scan the content.  And finally we used <b>expandable headed sections to keep content compact</b> giving Mady the option to explore details as needed.</p>

            <h4>PROPOSED LAYOUT: 45% LESS SCROLLING ON HOME PAGE</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: ProposedLayoutImage,*/}
            {/*    alt:'Proposed Layout Image',*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: ProposedLayoutImage,*/}
            {/*    alt:'Proposed Layout Image',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={ProposedLayoutImage} alt='Proposed Layout Image' />
            </ImageZoom>

            <div className={'sub'}><h3>CHECKOUT FLOW WITH CART</h3></div>
            <p>Nectar site currently takes Mady straight to the checkout page when she adds an item to the cart.  This <b>creates confusion</b> because she expects from online shopping experience that her item should be added to a shopping cart.  It also <b>disrupts her shopping experience</b> because she has to go back from the checkout page if she wants to continue shopping.  This is frustrating to her.</p>
            <h4>CURRENT CHECKOUT FLOW</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: CheckoutFlowCurrentImage,*/}
            {/*    alt:'Checkout Flow Current Image',*/}
            {/*    style:{marginBottom:'2rem'}*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: CheckoutFlowCurrentImage,*/}
            {/*    alt:'Checkout Flow Current Image',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={CheckoutFlowCurrentImage} alt='Checkout Flow Current Image' style={{marginBottom:'2rem'}}/>
            </ImageZoom>

            <h4>PROPOSED CHECKOUT FLOW</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: CheckoutFlowProposedImage,*/}
            {/*    alt:'Checkout Flow Proposed Image',*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: CheckoutFlowProposedImage,*/}
            {/*    alt:'Checkout Flow Proposed Image',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={CheckoutFlowProposedImage} alt='Checkout Flow Proposed Image'/>
            </ImageZoom>

            <div className={'sub'}><h3>USABILITY TESTING (PROPOSED PROTOTYPE)</h3></div>
            <p>Live usability tests were conducted with 5 users on a high-fidelity prototype at <a href='http://www.usertesting.com' target='blank'>usertesting.com</a> to validate design decisions.  Users completed all 3 tasks given without any of the issues they experienced in the current site usability tests.  Results in comparison are dramatically more positive. </p>
            <h4>HOMEPAGE (PROTOTYPE)</h4>

            <div style={{width:'350px', margin: '0 auto', marginBottom:'1.666rem'}}>
              {/*<ImageZoom*/}
              {/*  image={{*/}
              {/*    src: Prototype,*/}
              {/*    alt:'Prototype Image'*/}
              {/*  }}*/}
              {/*  zoomImage={{*/}
              {/*    src: Prototype,*/}
              {/*    alt:'Prototype',*/}
              {/*  }}*/}
              {/*/>*/}
              <ImageZoom>
                <img src={Prototype} alt='Prototype Image'/>
              </ImageZoom>
            </div>


            <h4>LIKERT SCALE (PROTOTYPE)</h4>
            {/*<ImageZoom*/}
            {/*  image={{*/}
            {/*    src: LikertPrototypeImage,*/}
            {/*    alt:'Likert Prototype',*/}
            {/*    style:{margin:'0 auto', marginBottom:'1.666rem'}*/}
            {/*  }}*/}
            {/*  zoomImage={{*/}
            {/*    src: LikertPrototypeImage,*/}
            {/*    alt:'Likert Prototype',*/}
            {/*  }}*/}
            {/*/>*/}
            <ImageZoom>
              <img src={LikertPrototypeImage} alt='Likert Prototype' style={{margin:'0 auto', marginBottom:'1.666rem'}}/>
            </ImageZoom>

            <h4>USER QUOTES (PROTOTYPE)</h4>
            <Slider {...{
              dots: false,
              arrows: true,
              speed: 500,
              fade: true,
              cssEase: 'linear',
              autoplay: true,
              autoplaySpeed: 5000,
              infinite: true,
              className:'slides'
            }}>
              <div className={'quotes'}>
                <blockquote><p>This website seems like there was a lot of <br/>consideration for mobile experience.</p></blockquote>
              </div>
              <div className={'quotes'}>
                <blockquote><p>It feels like a full blown experience on mobile <br/>which is what I want.</p></blockquote>
              </div>
              <div className={'quotes'}>
                <blockquote><p>I like how everything was laid out, especially on home page.<br/>A lot of my immediate questions were answered.</p></blockquote>
              </div>
            </Slider>


            <div className={'sub'}><h3>KEY TAKEAWAY</h3></div>
            <p>One of the key insights from our user interviews was that <b>most prefer to shop on mobile apps rather than on mobile web sites</b>.  They complained of the <b>incessant scrolling and irrelevant content</b>.  Once we addressed this issue by our redesigned mobile experience - the response turned around completely.  This shows that <b>content and layout design should be device specific</b> and not just responsive dimension of the desktop content.</p>

            </MainCopy>
          </div>
          <div className={'col1'}>
            <div style={{marginTop:'2px'}}>
              <Infobar title="UX Methods" items={uxMethods}/>
              <Infobar title="Team" items={team}/>
              <Infobar title="My Roles" items={['UX Researcher','UX/UI Design']}/>
              <Infobar title="Tools" items={['Sketch', 'InVision', 'Adobe CS']}/>
              <Infobar title="Timing" items={['2.5 Weeks Sprint']}/>
            </div>
          </div>
        </div>
      </div>
  </Layout>
)

// export const imageQuery = graphql`
//   query {
//     discoverHero: file(relativePath: { eq: "portfolio/discover/hero-discover@2x.jpg" }) {
//       childImageSharp {
//         fixed(width: 1440) {
//           ...GatsbyImageSharpFixed
//         }
//       }
//     }
//   }
// `

export default Nectar