import React from 'react';
import styled from "@emotion/styled";
import Layout from '../../components/layout'
import VideoPlayer from '../../components/video-player'
import Poster from '../../video/kraft.png'
import Video from '../../video/kraft.mp4';
import Breadcrumb from '../../components/breadcrumb'
import SideNav from '../../components/sidenav';
import MainCopy from '../../components/maincopy';
import ExternalLink from '../../components/external-link';
import Infobar from '../../components/infobar';
import Hero from '../../components/hero';
import HeroImage from '../../images/code/kraft-hero2.jpg';

// const Hero = styled('div') `
//   border-top: 1px solid rgb(220,220,220);
//   border-bottom: 1px solid rgb(220,220,220);
// `

class Kraft extends React.Component {
  render () {
    return (
      <Layout>

        {/*<Hero>*/}
          {/*<VideoPlayer poster={Poster} src={Video}/>*/}
        {/*</Hero>*/}

        <Hero image={HeroImage} alt='kraft hero'/>

        <div className={'row'} style={{paddingTop: '1rem'}}>
          <div className={'col1'}>
            <Breadcrumb section="portfolio" page="kraft"/>
          </div>

          <div className={'col2'}>&nbsp;</div>

          <div className={'col1'}>&nbsp;
            {/*<div style={{float:'right'}}>*/}
              {/*<ExternalLink data={{*/}
                {/*link: "http://www.rehmanidigital.com/clients/kraft/holiday/demo/",*/}
                {/*label: 'view project (flash)'*/}
              {/*}}/>*/}
            {/*</div>*/}

            {/*<div className={"clear"} />*/}
          </div>
        </div>

        <div className={'row'} style={{paddingTop: '1rem'}}>
          <div className={'col1'}>
            <div style={{marginLeft:'-0.5rem', marginTop: '2px'}}>
              <SideNav type = {SideNav.PORTFOLIO}/>
            </div>
          </div>
          <div className={"col2"} style={{height:'100vh'}}>
            <MainCopy>
              <h2>Project Overview</h2>
              <p>Kraft holiday site built around Ritz crackers with an extensive multimedia showcase including a multi-user shooting game, promotional giveaways, and engaging videos.</p>
              <p>As a tech lead, I provided technical guidance and client support through out the project.  I was also responsible for the site architecture, core front-end development, all animations and team management.</p>
              <h4 style={{marginBottom:'1rem'}}>PROJECT VIDEO</h4>
              <VideoPlayer poster={Poster} src={Video}/>
              <hr style={{marginTop:'1rem'}}/>
            </MainCopy>
          </div>

          <div className={'col1'}>
            <div style={{marginTop:'2px'}}>
              <Infobar title="Client/Agency" items={['Kraft / Havas']}/>
              <Infobar title="Roles" items={['Tech Lead', 'Creative Developer']}/>
              <Infobar title="Technical" items={['ActionScript', 'PixLib Framework', 'Flash Builder', 'Ant', 'Flash', 'Photoshop', 'Illustrator', 'HTML', 'JavaScript', 'XML']}/>
            </div>
          </div>

          <div className={"clear"}/>
        </div>
      </Layout>
    )
  }
}

export default Kraft