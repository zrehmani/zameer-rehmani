import React from 'react'

import Layout from '../components/layout'
import styled from '@emotion/styled'

const Header = styled('h1') ({
  padding: "3.2rem 5rem 0 5rem"
})

const DesignPage = () => (
  <Layout>
    <Header>DESIGN</Header>
  </Layout>
)

export default DesignPage
