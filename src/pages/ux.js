import React from 'react'
import Layout from '../components/layout'
import DiscoverImage from '../images/airbnb-discover.png';
import NectarImage from '../images/nectarsleep.png';
import LowesImage from '../images/lowes.png';
import SharedStreamsImage from '../images/sharedstreams.png';
import UXPreview from '../components/ux/ux-preview'

//data
const data1 = {
  link:'/ux/discover',
  copy1:'A mobile app design that help users find travel recommendations they can trust.',
  // copy1:'Design for a new mobile app that helps user discover what their friends are doing around the world.',
  copy2:'Build a social platform that helps user discover what their friends are doing around the world.',
  img:DiscoverImage,
  label:'Airbnb Discover'
}
const data2 = {
  link:'/ux/nectar/',
  copy1:'A redesign of Nectar\'s mobile ecommerce website that improves check out and conversion rates.',
  copy2:'Improves navigation, user flow and layout design to help user find and purchase multiple products.',
  img:NectarImage,
  label:'Nectarsleep'
}
const data3 = {
  link:'/ux/sharedstreams/',
  copy1:'A new Spotify feature that let users collaborate their music in real time.',
  copy2:'Seamlessly integrates the new feature that is easy for user to discover and use.',
  img:SharedStreamsImage,
  label:'Spotify',
  label2:'Shared Stream'
}
const data4 = {
  link:'/ux/lowes/',
  copy1:'A redesign of Lowe\'s primary website.',
  copy2:'Updates navigation and layout design to improve customer flow and engagement.',
  img:LowesImage,
  label:'Lowe\'s'
}

const UXPage = () => (
  <Layout>
    <UXPreview data={data1} odd/>
    <UXPreview data={data2} />
    <UXPreview data={data3} odd/>
    <UXPreview data={data4} />
  </Layout>
)

export default UXPage